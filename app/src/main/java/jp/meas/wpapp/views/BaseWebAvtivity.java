package jp.meas.wpapp.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.List;

import jp.meas.wpapp.BuildConfig;
import jp.meas.wpapp.R;
import jp.meas.wpapp.server.HttpdService;

/**
 * Created by nobu on 2016/11/20.
 */

public abstract class BaseWebAvtivity extends Activity implements GestureDetector.OnGestureListener{
    protected WebView webView_ = null;
    protected Handler hander_ = null;
    protected int progressValue_ = -1;
    protected GestureDetector gestureDetector_ = null;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initWebView() {
        webView_ = (WebView)findViewById(R.id.webView);
        webView_.getSettings().setJavaScriptEnabled(true);
        webView_.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView_.getSettings().setDefaultTextEncodingName("UTF-8");
        webView_.getSettings().setLoadWithOverviewMode(true);
        webView_.getSettings().setUseWideViewPort(true);
        webView_.getSettings().setDomStorageEnabled(true);
        webView_.setVerticalScrollbarOverlay(true);
        webView_.setVerticalScrollBarEnabled(false);
        webView_.setHorizontalScrollbarOverlay(true);
        webView_.setHorizontalScrollBarEnabled(false);
        webView_.setInitialScale(100);
        // DEBUG
        if(BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            webView_.setWebContentsDebuggingEnabled(true);
        }
        webView_.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
            @Override
            public void onProgressChanged(WebView view, int progress) {
                progressValue_ = progress;
            }
            @Override
            public  boolean onConsoleMessage (ConsoleMessage consoleMessage) {
//                System.out.printf(">>>\n(%s)%s\n", consoleMessage.messageLevel().toString() , consoleMessage.message());
                Log.d("WV-Cosole:"+consoleMessage.messageLevel().toString(), consoleMessage.message()+" <"+consoleMessage.lineNumber()+":"+consoleMessage.sourceId()+">");
                return true;
            }
        });
        gestureDetector_ = new GestureDetector(this, this);
        webView_.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector_.onTouchEvent(event);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode != KeyEvent.KEYCODE_BACK){
            return super.onKeyDown(keyCode, event);
        }else{
            // URLを見て戻し先決める
            if(webView_.canGoBack()) {
                Uri uri = Uri.parse(webView_.getUrl());
                List<String> paths = uri.getPathSegments();
                if(paths != null && paths.size() > 0) {
                    String firstPath = paths.get(0);
                    if(firstPath == null || firstPath.length() < 1 || paths.size() <= 1) {
                        finish();
                        if(this instanceof SlidInActivity) {
                            overridePendingTransition(R.anim.out_right, R.anim.out_right);
                        }else if(this instanceof PopUpActivity) {
                            overridePendingTransition(R.anim.out_down, 0);
                        }
                    }else{
                        if(paths.size() > 1 && !firstPath.equals("undefined")) {
                            webView_.goBack();
                        }
                    }
                }else{
                    String documentRoot = String.format("http://localhost:%d/index.html", HttpdService.serverPort);
                    if(uri.toString().equals(documentRoot)) {
                        finish();
                        if(this instanceof SlidInActivity) {
                            overridePendingTransition(R.anim.out_right, R.anim.out_right);
                        }else if(this instanceof PopUpActivity) {
                            overridePendingTransition(R.anim.out_down, 0);
                        }
                    }else{
                        webView_.goBack();
                    }
                }
                return true;
            }else{
                finish();
                if(this instanceof SlidInActivity) {
                    overridePendingTransition(R.anim.out_right, R.anim.out_right);
                }else if(this instanceof PopUpActivity) {
                    overridePendingTransition(R.anim.out_down, 0);
                }
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onPause() {
        webView_.loadUrl("javascript:terminateMovie()");
        try {
            WebView.class.getMethod("onPause").invoke(webView_);
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
        super.onPause();
    }

    @Override
    public void onStop(){
        webView_.loadUrl("javascript:terminateMovie()");
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        webView_.loadUrl("javascript:terminateMovie()");
        super.onDestroy();
    }

    @Override //毎起動時の処理
    protected void onResume() {
        super.onResume();
        hander_ = new Handler();
        try {
            WebView.class.getMethod("onResume").invoke(webView_);
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    @Override
    abstract public boolean onDown(MotionEvent e) ;

    @Override
    abstract public void onShowPress(MotionEvent e);

    @Override
    abstract public boolean onSingleTapUp(MotionEvent e);

    @Override
    abstract public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);

    @Override
    abstract public void onLongPress(MotionEvent e);

    @Override
    abstract public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);
}
