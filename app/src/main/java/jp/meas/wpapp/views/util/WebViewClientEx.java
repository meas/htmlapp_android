package jp.meas.wpapp.views.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.webkit.WebView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.meas.wpapp.AppCMSApplication;
import jp.meas.wpapp.MainActivity;
import jp.meas.wpapp.R;
import jp.meas.wpapp.server.AlarmReceiver;
import jp.meas.wpapp.server.HttpAccesser;
import jp.meas.wpapp.util.DBUtil;
import jp.meas.wpapp.util.NetworkUtils;
import jp.meas.wpapp.views.PopUpActivity;
import jp.meas.wpapp.views.SettingActivity;
import jp.meas.wpapp.views.SlidInActivity;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

/**
 * Created by nobu on 2016/11/20.
 */

public class WebViewClientEx {
    protected Activity activity_;
    protected Handler handler_;
    private Messenger replyMessanger_;

    final public static String NOTIFICATION_EXTRA_KEY = "notid";

    static public class ReplyHander extends Handler {

        WebView view_ = null;
        String callbackKey_ = null;
        DBUtil.NotificationTable data_ = null;
        int updateCount_ = -1;
        boolean repeat_ = false;

        public ReplyHander(WebView view, String callbackKey, DBUtil.NotificationTable data) {
            view_ = view;
            callbackKey_ = callbackKey;
            data_ = data;
        }

        public ReplyHander(WebView view, String callbackKey, int updateCount) {
            view_ = view;
            callbackKey_ = callbackKey;
            updateCount_ = updateCount;
        }

        public ReplyHander(WebView view, String callbackKey, DBUtil.NotificationTable data, boolean repeat) {
            view_ = view;
            callbackKey_ = callbackKey;
            data_ = data;
            repeat_ = repeat;
        }

        public ReplyHander(WebView view, String callbackKey, int updateCount, boolean repeat) {
            view_ = view;
            callbackKey_ = callbackKey;
            updateCount_ = updateCount;
            repeat_ = repeat;
        }

        public void sendResult(WebView view, String callbackKey, DBUtil.NotificationTable data) {
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(data.toJson())), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(data.toJson())), null);
            }
        }
        public void sendResult(WebView view, String callbackKey, int updateCount) {
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s, '{\"count\":%d}');", callbackKey, updateCount), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s, '{\"count\":%d}');", callbackKey, updateCount), null);
            }
        }
        public void sendResultError(WebView view, String callbackKey, int resultCode) {
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s, '{\"error\":%d}');", callbackKey, resultCode), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s, '{\"error\":%d}');", callbackKey, resultCode), null);
            }
        }

        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case 0:
                    sendResult(view_, callbackKey_, data_);
                    break;
                case 1:
                    sendResult(view_, callbackKey_, updateCount_);
                    break;
                default:
                    sendResultError(view_, callbackKey_, msg.what);
                    break;
            }
        }
    }

    public WebViewClientEx(Activity activity) {
        activity_ = activity;
        handler_ = new Handler();
    }

    static public String escapeArg(String arg) {
        arg = arg.replaceAll("\n", "");
        return arg.replaceAll("'", "\\'");
    }

    static public String escapeArgDQ(String arg, boolean deep) {
        arg = arg.replaceAll("\n", "");
        arg = arg.replaceAll("\r", "");
        arg =  arg.replaceAll("'", "\\\\'");
        if(deep) {
            return arg.replaceAll("\"", "\\\\\\\\\"");
        }
        return arg.replaceAll("\"", "\\\\\"");
    }

    static public String escapeArgDQ(String arg) {
        return escapeArgDQ(arg, false);
    }

    static public String decodeURL(String value) {
        if(value != null) {
            try {
                value = value.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
                value = value.replaceAll("\\+", "%2B");
                return  URLDecoder.decode(value, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public boolean shouldOverrideUrlLoading(final WebView view, String url) {
        Uri uri = Uri.parse(url);
        String scheme = uri.getScheme();
        String domain = uri.getHost();
        List<String> paths = uri.getPathSegments();
        int port = Uri.parse(view.getUrl()).getPort();
        if (scheme.equals("setting")) {
            Intent i = new Intent(activity_.getApplicationContext(), SettingActivity.class);
            activity_.startActivity(i);
            return true;
        } else if (scheme.equals("eula")) {
            String urlStr = activity_.getResources().getString(R.string.eula_url);
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(urlStr));
            activity_.startActivity(i);
            return true;
        } else if (scheme.equals("privacy")) {
            String urlStr = activity_.getResources().getString(R.string.privacyURL);
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(urlStr));
            activity_.startActivity(i);
            return true;
        } else if (scheme.equals("bottominbrowser") || scheme.equals("bottominbrowsers")) {
            String urlStr = null;
            if(scheme.equals("bottominbrowser")) {
                urlStr = String.format("http%s", url.substring(url.indexOf(":")));
            }else{
                urlStr = String.format("https%s", url.substring(url.indexOf(":")));
            }
            if(domain.equals("localhost")) {
                if(port > 0 && port != 80) {
                    urlStr = urlStr.replace("localhost", String.format("localhost:%d", port));
                }
            }
            Intent i = new Intent(activity_.getApplicationContext(), PopUpActivity.class);
            i.putExtra("URL", urlStr);
            activity_.startActivity(i);
            activity_.overridePendingTransition(R.anim.in_up, 0);
            return true;
        } else if (scheme.equals("rightinbrowser") || scheme.equals("rightinbrowsers")) {
            String urlStr = null;
            if(scheme.equals("rightinbrowser")) {
                urlStr = String.format("http%s", url.substring(url.indexOf(":")));
            }else{
                urlStr = String.format("https%s", url.substring(url.indexOf(":")));
            }
            if(domain.equals("localhost")) {
                if(port > 0 && port != 80) {
                    urlStr = urlStr.replace("localhost", String.format("localhost:%d", port));
                }
            }
            Intent i = new Intent(activity_.getApplicationContext(), SlidInActivity.class);
            i.putExtra("URL", urlStr);
            activity_.startActivity(i);
            activity_.overridePendingTransition(R.anim.in_right, R.anim.in_right);
            return true;
        } else if (scheme.equals("pop") || scheme.equals("dissmiss")) {
            activity_.finish();
            if(activity_ instanceof SlidInActivity) {
                activity_.overridePendingTransition(R.anim.out_right, R.anim.out_right);
            }else if(activity_ instanceof PopUpActivity) {
                activity_.overridePendingTransition(R.anim.out_down, 0);
            }
            return true;
        } else if (scheme.equals("ttp") || scheme.equals("ttps")) {
            if(!checkHasApplication(url)) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("h"+url));
                activity_.startActivity(i);
            }
            return true;
        } else if (scheme.equals("notification")) {
            if(!AlarmReceiver.getNotificationStatus(activity_)) { //通知がオフの場合はアラート
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity_);
                alertDialogBuilder.setTitle(activity_.getString(R.string.notification_on_title));
                alertDialogBuilder.setMessage(activity_.getString(R.string.notification_on_message));
                alertDialogBuilder.setPositiveButton(activity_.getString(R.string.notification_on_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.setCancelable(false);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
            }
            if(paths.size() > 0) {
                String control = paths.get(0).toLowerCase();
                String query = uri.getQuery();
                Map<String, String> keyValue = parseQuery(query);
                final String callbackKey = keyValue.get("key");
                AlarmManager alarmManager = (AlarmManager)activity_.getSystemService(Context.ALARM_SERVICE);
                if("add".equals(control)) {
                    String stId = decodeURL(keyValue.get("stId"));
                    int storageId = stId != null ? Integer.parseInt(stId) : -1;
                    String title = decodeURL(keyValue.get("title"));
                    String text = decodeURL(keyValue.get("text"));
                    String subtext = decodeURL(keyValue.get("subtext"));
                    String date = decodeURL(keyValue.get("date"));
                    String repeat = decodeURL(keyValue.get("repeat"));
                    DBUtil db = new DBUtil(activity_);
                    DBUtil.NotificationTableAccesser nta = db.getNotificationTableWritable();
                    DBUtil.NotificationTable data = nta.insert(storageId, title, text, subtext, date, Boolean.valueOf(repeat));
                    try {
                        Message msg = Message.obtain(null, data.id, "add");
                        replyMessanger_ = new Messenger(new ReplyHander(view, callbackKey, data));
                        msg.replyTo = replyMessanger_;
                        MainActivity.messengerNotificaiton_.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        //TODO エラー処理（通史設定失敗）
                        if(Build.VERSION.SDK_INT >= 19) {
                            view.evaluateJavascript(String.format("javascript:pop(%s, '{\"error\":%d, \"message\":\"%s\"}');", callbackKey, 99, e.getLocalizedMessage()), null);
                        }else{
                            view.loadUrl(String.format("javascript:pop(%s, '{\"error\":%d, \"message\":\"%s\"}');", callbackKey, 99, e.getLocalizedMessage()), null);
                        }
                    }
                }else if("mod".equals(control)) {
                    String id = decodeURL(keyValue.get("id"));
                    String stId = decodeURL(keyValue.get("stId"));
                    String title = decodeURL(keyValue.get("title"));
                    String text = decodeURL(keyValue.get("text"));
                    String subtext = decodeURL(keyValue.get("subtext"));
                    String date = decodeURL(keyValue.get("date"));
                    String repeat = decodeURL(keyValue.get("repeat"));
                    DBUtil db = new DBUtil(activity_);
                    DBUtil.NotificationTableAccesser nta = db.getNotificationTableWritable();
                    int updateCount = nta.update(Integer.parseInt(id), Integer.parseInt(stId), title, text, subtext, date, Boolean.valueOf(repeat));
                    //
                    if(updateCount > 0) {
                        Intent intent = new Intent(activity_.getApplicationContext(), AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity_.getApplicationContext(), Integer.parseInt(id), intent, FLAG_CANCEL_CURRENT);
                        pendingIntent.cancel();
                        alarmManager.cancel(pendingIntent);
                    }
                    //
                    try {
                        Message msg = Message.obtain(null, Integer.parseInt(id), "mod");
                        replyMessanger_ = new Messenger(new ReplyHander(view, callbackKey, updateCount));
                        msg.replyTo = replyMessanger_;
                        MainActivity.messengerNotificaiton_.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        //TODO エラー処理（通史設定失敗）
                        if(Build.VERSION.SDK_INT >= 19) {
                            view.evaluateJavascript(String.format("javascript:pop(%s, '{\"error\":%d, \"message\":\"%s\"}');", callbackKey, 99, e.getLocalizedMessage()), null);
                        }else{
                            view.loadUrl(String.format("javascript:pop(%s, '{\"error\":%d, \"message\":\"%s\"}');", callbackKey, 99, e.getLocalizedMessage()), null);
                        }
                    }
                }else if("del".equals(control)) {
                    String id = decodeURL(keyValue.get("id"));
                    DBUtil db = new DBUtil(activity_);
                    DBUtil.NotificationTableAccesser nta = db.getNotificationTableWritable();
                    int updateCount = nta.delete(Integer.parseInt(id));
                    //
                    if(updateCount > 0) {
                        Intent intent = new Intent(activity_.getApplicationContext(), AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity_.getApplicationContext(), Integer.parseInt(id), intent, FLAG_CANCEL_CURRENT);
                        pendingIntent.cancel();
                        alarmManager.cancel(pendingIntent);
                    }
                    if(Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(String.format("javascript:pop(%s, '{\"count\":%d}');", callbackKey, updateCount), null);
                    }else{
                        view.loadUrl(String.format("javascript:pop(%s, '{\"count\":%d}');", callbackKey, updateCount), null);
                    }
                }else if("get".equals(control)) {
                    String sql = decodeURL(keyValue.get("sql"));
                    DBUtil db = new DBUtil(activity_);
                    DBUtil.NotificationTableAccesser nta = db.getNotificationTableReadable();
                    List<DBUtil.NotificationTable> datas = nta.select(sql);
                    //
                    StringBuffer jsonStr = new StringBuffer();
                    jsonStr.append("[");
                    for(DBUtil.NotificationTable data : datas) {
                        if(jsonStr.length() > 1) {
                            jsonStr.append(",");
                        }
                        jsonStr.append(data.toJson());
                    }
                    jsonStr.append("]");
                    if(Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(jsonStr.toString())), null);
                    }else{
                        view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(jsonStr.toString())), null);
                    }
                }
            }
            return true;
        } else if (scheme.equals("gatrack")) {
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            String callbackKey = keyValue.get("key");
            String title = decodeURL(keyValue.get("title"));
            Tracker t = ((AppCMSApplication)activity_.getApplication()).getTracker();
            t.setScreenName(title);
            t.send(new HitBuilders.AppViewBuilder().build());
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s);", callbackKey), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s);", callbackKey), null);
            }
            return true;
        } else if (scheme.equals("gaevent")) {
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            String callbackKey = keyValue.get("key");
            String category = decodeURL(keyValue.get("category"));
            String action = decodeURL(keyValue.get("action"));
            String label = decodeURL(keyValue.get("label"));
            String screen = decodeURL(keyValue.get("screen"));
            Tracker t = ((AppCMSApplication)activity_.getApplication()).getTracker();
            t.setScreenName(screen);
            HitBuilders.EventBuilder eb = new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .setValue(0);
            t.send(eb.build());
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s);", callbackKey), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s);", callbackKey), null);
            }
            return true;
        } else if (scheme.equals("dbaccess")) {
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            String callbackKey = keyValue.get("key");
            String sql = decodeURL(keyValue.get("sql"));
            if(sql.toLowerCase().trim().startsWith("select")) {
                DBUtil db = new DBUtil(activity_);
                DBUtil.DataTableAccesser dta = db.getDataTableReadable();
                List<DBUtil.DataTable> datas = dta.select(sql);
                StringBuffer jsonStr = new StringBuffer();
                jsonStr.append("[");
                for(DBUtil.DataTable data : datas) {
                    if(jsonStr.length() > 1) {
                        jsonStr.append(",");
                    }
                    jsonStr.append(data.toJson());
                }
                jsonStr.append("]");
                if(Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(jsonStr.toString())), null);
                }else{
                    view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(jsonStr.toString())), null);
                }
            }else if(sql.toLowerCase().trim().startsWith("insert")) {
                DBUtil db = new DBUtil(activity_);
                DBUtil.DataTableAccesser dta = db.getDataTableWritable();
                DBUtil.DataTable data = dta.insert(sql);
                if(Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(data.toJson())), null);
                }else{
                    view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(data.toJson())), null);
                }
            }else{
                DBUtil db = new DBUtil(activity_);
                DBUtil.DataTableAccesser dta = db.getDataTableWritable();
                boolean result = dta.update(sql);
                if(Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, String.valueOf(result)), null);
                }else{
                    view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, String.valueOf(result)), null);
                }
            }
            return true;
        } else if (scheme.equals("ajax")) {
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            String method = keyValue.get("method").toLowerCase();
            String json = decodeURL(keyValue.get("data"));
            final String callbackKey = keyValue.get("key");
            URL sendURL = null;
            try {
                if("get".equals(method)) {
                    if(json != null && json.length() > 0) {
                        sendURL = new URL(decodeURL(keyValue.get("url"))+"&"+json);
                    }else{
                        sendURL = new URL(decodeURL(keyValue.get("url")));
                    }
                }else {
                    sendURL = new URL(decodeURL(keyValue.get("url")));
                }
            }catch (MalformedURLException e) {
                e.printStackTrace();
                if(Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                }else{
                    view.loadUrl(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                }
                return true;
            }

            HttpAccesser http = null;
            if("get".equals(method)) {
                http = new HttpAccesser(activity_, sendURL, HttpAccesser.GET, false, new HttpAccesser.HttpAccessResponse() {
                    @Override
                    public boolean needHeader() {
                        return false;
                    }

                    @Override
                    public boolean header(URL url, Map<String, List<String>> headers, Exception e) {
                        return true;
                    }

                    @Override
                    public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser) {
                        try {
                            final int rcode = response.getStatusLine().getStatusCode();
                            int len = 0;
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            BufferedOutputStream bos = new BufferedOutputStream(baos);
                            byte[] buffer = new byte[1024];
                            while ((len = is.read(buffer)) != -1) {
                                bos.write(buffer, 0, len);
                            }
                            bos.flush();
                            bos.close();
                            final String rJson = new String(baos.toByteArray(), "UTF-8");
                            boolean isJson = true;
                            try {
                                String debug = escapeArgDQ(rJson);
                                JSONObject test = new JSONObject(rJson);
                            } catch (JSONException e) {
                                isJson = false;
                            }
                            if(isJson) {
                                handler_.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(Build.VERSION.SDK_INT >= 19) {
                                            view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":%d, \"data\":%s}');", callbackKey, rcode, escapeArgDQ(rJson)), null);
                                        }else{
                                            view.loadUrl(String.format("javascript:pop(%s, '{\"result\":%d, \"data\":%s}');", callbackKey, rcode, escapeArgDQ(rJson)), null);
                                        }
                                    }
                                });
                            }else{
                                handler_.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(Build.VERSION.SDK_INT >= 19) {
                                            view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":%d, \"data\":\"%s\"}');", callbackKey, rcode, escapeArgDQ(rJson, true)), null);
                                        }else{
                                            view.loadUrl(String.format("javascript:pop(%s, '{\"result\":%d, \"data\":\"%s\"}');", callbackKey, rcode, escapeArgDQ(rJson, true)), null);
                                        }
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            handler_.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(Build.VERSION.SDK_INT >= 19) {
                                        view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                                    }else{
                                        view.loadUrl(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                                    }
                                }
                            });
                        }
                        return true;
                    }

                    @Override
                    public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser) {
                        //TODO?
                    }
                });
                HashMap<String, String>[] params = new HashMap[1];
                params[0] = new HashMap<String, String>();
                http.execute(params);
            }else if("post".equals(method) || "put".equals(method) || "delete".equals(method)) {
                int reqMethod = HttpAccesser.POST;
                if("put".equals(method)) {
                    reqMethod = HttpAccesser.PUT;
                }else if("delete".equals(method)) {
                    reqMethod = HttpAccesser.DELETE;
                }
                http = new HttpAccesser(activity_, sendURL, reqMethod, false, new HttpAccesser.HttpAccessResponse() {
                    @Override
                    public boolean needHeader() {
                        return false;
                    }

                    @Override
                    public boolean header(URL url, Map<String, List<String>> headers, Exception e) {
                        return true;
                    }

                    @Override
                    public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser) {
                        try {
                            final int rcode = response.getStatusLine().getStatusCode();
                            int len = 0;
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            BufferedOutputStream bos = new BufferedOutputStream(baos);
                            byte[] buffer = new byte[1024];
                            while ((len = is.read(buffer)) != -1) {
                                bos.write(buffer, 0, len);
                            }
                            bos.flush();
                            bos.close();
                            final String rJson = new String(baos.toByteArray(), "UTF-8");
                            handler_.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(Build.VERSION.SDK_INT >= 19) {
                                        view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":%d, \"data\":%s}');", callbackKey, rcode, escapeArgDQ(rJson)), null);
                                    }else{
                                        view.loadUrl(String.format("javascript:pop(%s, '{\"result\":%d, \"data\":%s}');", callbackKey, rcode, escapeArgDQ(rJson)), null);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            handler_.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(Build.VERSION.SDK_INT >= 19) {
                                        view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                                    }else{
                                        view.loadUrl(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                                    }
                                }
                            });
                        }
                        return true;
                    }

                    @Override
                    public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser) {

                    }
                });
                HashMap<String, String>[] params = new HashMap[1];
                params[0] = new HashMap<String, String>();
                params[0].put("", json);
                http.execute(params);
            }
            return true;
        } else if (scheme.equals("alert")) {
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            String title = keyValue.get("title");
            String message = keyValue.get("message");
            String buttonName = keyValue.get("buttonName") != null ? keyValue.get("buttonName") : "閉じる";
            final String callbackKey = keyValue.get("key");

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity_);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setPositiveButton(buttonName, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if(Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(String.format("javascript:pop(%s);", callbackKey), null);
                    }else{
                        view.loadUrl(String.format("javascript:pop(%s);", callbackKey), null);
                    }
                }
            });
            alertDialogBuilder.setCancelable(false);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return true;
        } else if (scheme.equals("confirm")) {
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            String title = keyValue.get("title");
            String message = keyValue.get("message");
            String buttonName = keyValue.get("buttonName") != null ? keyValue.get("buttonName") : "OK";
            String buttonCancel = keyValue.get("buttonCancel") != null ? keyValue.get("buttonCancel") : "キャンセル";
            String others = keyValue.get("others") != null ? keyValue.get("others") : null;

            final String callbackKey = keyValue.get("key");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity_);
            alertDialogBuilder.setTitle(title);
            if (others != null && others.length() > 0) {
                final String otherList[] = others.split(",");
                if (otherList.length > 0) {
                    alertDialogBuilder.setSingleChoiceItems(otherList, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (Build.VERSION.SDK_INT >= 19) {
                                view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(otherList[which])), null);
                            } else {
                                view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, escapeArg(otherList[which])), null);
                            }
                        }
                    });
                } else {
                    alertDialogBuilder.setMessage(message);
                }
            } else {
                alertDialogBuilder.setMessage(message);
            }
            alertDialogBuilder.setPositiveButton(buttonName, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(String.format("javascript:pop(%s, 'OK');", callbackKey), null);
                    } else {
                        view.loadUrl(String.format("javascript:pop(%s, 'OK');", callbackKey), null);
                    }
                }
            });
            alertDialogBuilder.setNegativeButton(buttonCancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(String.format("javascript:pop(%s, 'cancel');", callbackKey), null);
                    } else {
                        view.loadUrl(String.format("javascript:pop(%s, 'cancel');", callbackKey), null);
                    }
                }
            });
            alertDialogBuilder.setCancelable(false);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return true;
        } else if (scheme.equals("gettoken")) {
            //TODO アクセストークンの取得
            //TODO
            //TODO
            //TODO
            //TODO
            //TODO
        } else if (scheme.equals("debugnotitificaitonon")) {
            AlarmReceiver.setNotifictionStatus(activity_, true);
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            final String callbackKey = keyValue.get("key");
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s);", callbackKey), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s);", callbackKey), null);
            }
            return true;
        } else if (scheme.equals("debugnotitificaitonff")) {
            AlarmReceiver.setNotifictionStatus(activity_, false);
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            final String callbackKey = keyValue.get("key");
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s);", callbackKey), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s);", callbackKey), null);
            }
            return true;
        } else if (scheme.equals("netcheck")) {
            Boolean isConnect = NetworkUtils.isNetworkConnect(activity_);
            String query = uri.getQuery();
            Map<String, String> keyValue = parseQuery(query);
            final String callbackKey = keyValue.get("key");
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:pop(%s, '%s');", callbackKey, isConnect.toString().toLowerCase()), null);
            }else{
                view.loadUrl(String.format("javascript:pop(%s, '%s');", callbackKey, isConnect.toString().toLowerCase()), null);
            }
            return true;
        } else if (scheme.equals("log")) {
            //TODO
            //TODO 未実装
            //TODO
            //TODO
            //TODO
            //TODO
            return true;
        }
        return false;
    }

    public void onPageFinished(WebView view , String url){
        PackageManager pm = activity_.getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageInfo(activity_.getPackageName(), 0);
            if(Build.VERSION.SDK_INT >= 19) {
                view.evaluateJavascript(String.format("javascript:var androidVersionCode='%d';", packageInfo.versionCode), null);
                view.evaluateJavascript(String.format("javascript:var androidVersionName='%s';", packageInfo.versionName), null);
            }else{
                view.loadUrl(String.format("javascript:var androidVersionCode='%d';", packageInfo.versionCode));
                view.loadUrl(String.format("javascript:var androidVersionName='%s';", packageInfo.versionName));
            }
        }
        catch (Exception e) {
        }
    }

    /**
     * 起動チェック
     * @param url
     * @return
     */
    protected boolean checkHasApplication(String url) {
        boolean result = false;
        return result;
    }

    protected Map<String, String>parseQuery(String query) {
        Map<String, String>result = new HashMap<String, String>();

        String params[] = query.split("&");
        for(String param : params) {
            String keyValue[] = param.split("=");
            String key = keyValue[0];
            String value = param.substring(param.indexOf("=")+1);
            result.put(key, value);
        }

        return result;
    }
}
