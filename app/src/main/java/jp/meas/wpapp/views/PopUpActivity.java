package jp.meas.wpapp.views;

import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import jp.meas.wpapp.R;
import jp.meas.wpapp.util.StorageAccesser;
import jp.meas.wpapp.views.util.WebViewClientEx;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 */
public class PopUpActivity extends BaseWebAvtivity {
    private String urlStr_ = null;
    private TextView textView = null;
    private String dID_ = null;
    private String uID_ = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_upview);
        initWebView();
        urlStr_ = getIntent().getStringExtra("URL");
    }

    @Override
    protected void onResume() {
        super.onResume();
        dID_ = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
        uID_ = StorageAccesser.getPreferenceString(StorageAccesser.KEY_UNIQUE_ID);
        String urlStr = null;
        textView = (TextView)findViewById(R.id.title);
        if(urlStr_ != null) {
            urlStr = urlStr_;
            textView.setText("");
        } else {
            finish();
            overridePendingTransition(R.anim.out_down, 0);
        }
        Button button = (Button)findViewById(R.id.backBtu);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.out_down, 0);
            }
        });

        webView_.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
            @Override
            public void onProgressChanged(WebView view, int progress) {
            }
            @Override
            public  boolean onConsoleMessage (ConsoleMessage consoleMessage) {
                return true;
            }
        });
        webView_.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                WebViewClientEx we = new WebViewClientEx(PopUpActivity.this);
                if(we.shouldOverrideUrlLoading(view, url)) {
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
            @Override
            public void onPageFinished(WebView view , String url){
                WebViewClientEx we = new WebViewClientEx(PopUpActivity.this);
                we.onPageFinished(view, url);
            }
        });
        webView_.loadUrl(urlStr);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }
    @Override
    public void onLongPress(MotionEvent e) {
    }
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }
    @Override
    public void onShowPress(MotionEvent e) {
    }
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);

        float absX = Math.abs(velocityX);
        float absY = Math.abs(velocityY);
        float distance = displayMetrics.widthPixels > 720 ? 3500f : 1800f;
        float pointY = displayMetrics.heightPixels - e1.getY();

        if(pointY > 230f) {
            if(velocityX > 0) { //swipeRight
                if(absX >= distance && absY < distance) {
                    if(Build.VERSION.SDK_INT >= 19) {
                        webView_.evaluateJavascript("javascript:swipeRight()", null);
                    }else{
                        webView_.loadUrl("javascript:swipeRight()");
                    }
                }
            }else{ //swipeLeft
                if(absX >= distance && absY < distance) {
                    if(Build.VERSION.SDK_INT >= 19) {
                        webView_.evaluateJavascript("javascript:swipeLeft()", null);
                    }else{
                        webView_.loadUrl("javascript:swipeLeft()");
                    }
                }
            }
        }
        return false;
    }
}
