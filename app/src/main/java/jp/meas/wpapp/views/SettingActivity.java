package jp.meas.wpapp.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.meas.wpapp.R;
import jp.meas.wpapp.server.HttpAccesser;
import jp.meas.wpapp.server.HttpAccesser.HttpAccessResponse;
import jp.meas.wpapp.util.CommonMethods;
import jp.meas.wpapp.util.StorageAccesser;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 */
public class SettingActivity extends Activity {
    public static final String NAME = "setting.json";
    private String urlStr_ = null;
    private WebView webView = null;
    private TextView textView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting);
        urlStr_ = getIntent().getStringExtra("URL");
    }

    @Override
    protected void onResume() {
        super.onResume();
        String dID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
        String uID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_UNIQUE_ID);
        String urlStr = null;
        textView = (TextView)findViewById(R.id.settingTitle);
        if(urlStr_ != null) {
            urlStr = urlStr_;
            textView.setText("");
        } else {
            urlStr = String.format("%s?device_id=%s&unique_id=%s&device=android&ver=%s",
                        getResources().getString(R.string.settingURL),
                        dID, uID,
                        getResources().getString(R.string.api_version));

        }
        Button button = (Button)findViewById(R.id.settingBackBtu);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSettingInfo(getApplication(), new SaveSetting());
                finish();
            }
        });

        webView = (WebView)findViewById(R.id.settingWeb);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDefaultTextEncodingName("UTF-8");
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setVerticalScrollbarOverlay(true);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollbarOverlay(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setInitialScale(100);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
            @Override
            public void onProgressChanged(WebView view, int progress) {
            }
            @Override
            public  boolean onConsoleMessage (ConsoleMessage consoleMessage) {
                return true;
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                String scheme = uri.getScheme();
                if (scheme.equals("ttp") || scheme.equals("ttps")) {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("h" + url));
                    startActivity(i);
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
            @Override
            public void onPageFinished(WebView view , String url){
                if(Build.VERSION.SDK_INT >= 19) {
                }else{
                }
            }
        });
        webView.loadUrl(urlStr);
    }

    public static void getSettingInfo(Context ctx, HttpAccessResponse callback) {
        if(!CommonMethods.isOnline()) {
            return;
        }
        try {
            URL url = new URL(ctx.getResources().getString(R.string.settingInfoURL));
            HttpAccesser http = new HttpAccesser(ctx, url, HttpAccesser.GET, false, callback);
            HashMap<String, String>[] params = null;
            String dID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
            String uID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_UNIQUE_ID);
            JSONObject regObj = new JSONObject();
            regObj.put("device_id", dID);
            regObj.put("unique_id", uID);
            regObj.put("ver", ctx.getString(R.string.api_version));
            JSONObject rootObj = new JSONObject();
            rootObj.put("regist", regObj);
            params = new HashMap[1];
            params[0] = new HashMap<String, String>();
            params[0].put("", rootObj.toString());
            http.execute(params);
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public static String getSettingJson() {
        String path = CommonMethods.getCashRootPath() + NAME;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
            StringBuffer buf = new StringBuffer();
            String str = null;
            while ((str = reader.readLine()) != null) {
                buf.append(str);
                buf.append("\n");
            }
            reader.close();
            return buf.toString();
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return  null;
    }

    public static class SaveSetting implements HttpAccessResponse {

        @Override
        public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser) {
            if(response.getStatusLine().getStatusCode() == 200) {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    StringBuffer buf = new StringBuffer();
                    String str = null;
                    while ((str = reader.readLine()) != null) {
                        buf.append(str);
                        buf.append("\n");
                    }
                    is.close();
                    String path = CommonMethods.getCashRootPath() + NAME;
                    File file = new File(path);
                    Log.v("SaveSetting", file.getAbsolutePath());
                    if(file.exists()) {
                        file.delete();
                    }
                    try {
                        FileOutputStream fis = new FileOutputStream(file);
                        OutputStreamWriter writer = new OutputStreamWriter(fis);
                        writer.write(buf.toString());
                        writer.close();
                    } catch (Exception e) {
                        e.printStackTrace(System.err);
                    }
                    Log.d("DEBUG", buf.toString());
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            } else {
                return false;
            }
        }

        @Override
        public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser) {

        }

        @Override
        public boolean needHeader() {
            return false;
        }

        @Override
        public boolean header(URL url, Map<String, List<String>> headers, Exception e) {
            return true;
        }
    }
}
