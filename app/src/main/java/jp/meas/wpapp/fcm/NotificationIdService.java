package jp.meas.wpapp.fcm;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by nobu on 2016/11/10.
 */

public class NotificationIdService extends FirebaseInstanceIdService {

    private static final String TAG = "NotificationIdService";
    public static String deviceId = null;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        deviceId = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + deviceId);

    }
}
