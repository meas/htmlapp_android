package jp.meas.wpapp;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.Date;
import java.util.UUID;

import jp.meas.wpapp.util.StorageAccesser;

public class AppCMSApplication extends Application {

    //TODO
    //https://fello.net/help/notification_reference_for_android#getRegistrationId

    private Tracker mTracker = null;

    public synchronized Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
    	super.onCreate();
	    StorageAccesser.initialize(this);
		String dID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
	    if((dID == null || dID.equals(""))){
            //UUIDに変更
            String udid = String.format("%s-%d", UUID.randomUUID().toString(), (new Date()).getTime());
		    StorageAccesser.setPreferenceString(StorageAccesser.KEY_DEVICE_ID, "");
		    StorageAccesser.setPreferenceString(StorageAccesser.KEY_UNIQUE_ID, udid);
		    //デバイス登録フラグをfalseに。MainActivityでチェックする
		    StorageAccesser.setDeviceRegistered(false);
	    }
    }
}
