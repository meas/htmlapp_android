package jp.meas.wpapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;


public class StorageAccesser {
	private static final StorageAccesser sa = new StorageAccesser();
	private static Context context_ = null;

	public static final String KEY_DEVICE_ID = "device_id";
	public static final String KEY_UNIQUE_ID = "unique_id";
	public static final String KEY_LAST_UPDATE = "content_last_update";
	private static SharedPreferences sp = null;


	public static final StorageAccesser getInstance(Context context) {
		context_ = context;
		return sa;
	}
	
	public void setSharedPreferences(String key, String value) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context_);
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	public String getSharedPreferences(String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context_);
		return sharedPreferences.getString(key, null);
	}

	public static void initialize(Context ctx){
		if(sp == null){
			sp = PreferenceManager.getDefaultSharedPreferences(ctx);
		}
	}

	public static String getPreferenceString(String key){
		return sp.getString(key, "");
	}

	public static void setPreferenceString(String key, String value){
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static Long getLastUpDate(){
		return sp.getLong(KEY_LAST_UPDATE, 0);
	}

	public static void setLastUpdate(String dateStr){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss"); //TODO newlist.csvの一行目を利用
		Date date = null;
		try {
			if(dateStr != null) {
				date = sdf.parse(dateStr);
			}
		}
		catch (Exception e) {
			//
		}
		long time = date != null ? date.getTime() : 0; //TODO newslist.csvの一行目取れなかったら”0”
		Editor editor = sp.edit();
		editor.putLong(KEY_LAST_UPDATE, time);
		editor.commit();
		if(dateStr != null) {
			Log.v("setLastUpdate:", "last time:" + dateStr);
		}else{
			Log.v("setLastUpdate:", "last time: none");
		}
	}

	public static boolean isDeviceRegistered(){
		return sp.getBoolean("device_registered", false);
	}

	public static void setDeviceRegistered(boolean value){
		Editor editor = sp.edit();
		editor.putBoolean("device_registered", value);
		editor.commit();
	}

}
