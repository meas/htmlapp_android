package jp.meas.wpapp.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jp.meas.wpapp.R;
import jp.meas.wpapp.server.HttpAccesser;
import jp.meas.wpapp.server.HttpdService;

/**
 * Created by nobu
 * 共通メソッド　static で呼び出す
 */
public class CommonMethods {

	private static Context ctx = null;
    private static boolean accessNetwork = false;
    private static boolean networkChecking = false;

	public static void initialize(Context context){
		ctx = context;
	}

	//ファイルパス関係 htdocs
	public static String getDocumentRootPath(){
		String rootStr = ctx.getResources().getString(R.string.document_root);
		String path = ctx.getFilesDir().getAbsolutePath() + "/" + rootStr + "/";
		return path;
	}

	//ファイルパス関係 _cache
	public static String getCashRootPath(){
		String cashStr = ctx.getResources().getString(R.string.cache_path);
		String path = CommonMethods.getDocumentRootPath() + cashStr + "/";
		return path;
	}

	//ファイルパス関係 localhost のキャッシュ
	public static String getLocalHostCashPath(){
		String cashStr = ctx.getResources().getString(R.string.cache_path);
		String path = String.format("http://localhost:%d/%s/", HttpdService.serverPort, cashStr);
		return path;
	}

	public static void createRootAndCacheDirectory(){
		String root = CommonMethods.getDocumentRootPath();
		File rootF = new File(root);
		if(rootF.exists() == false){
			rootF.mkdir();
		}
		String cachePath = CommonMethods.getCashRootPath();
		File cacheF = new File(cachePath);
		if(cacheF.exists() == false){
			cacheF.mkdir();
		}
	}

	//日付関係
	public static Date getYMDDateFromDateString(String dateStr){
		if(CommonMethods.checkStringIsBlank(dateStr)) return null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.JAPAN);
			Date date = sdf.parse(dateStr);
			return date;
		} catch (Exception e) {
//			if(BuildConfig.DEBUG) {
//				e.printStackTrace();
//			}
			return null;
		}
	}

	public static Date getDateFromDateString(String dateStr){
		if(CommonMethods.checkStringIsBlank(dateStr)) return null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmssZZZ", Locale.JAPAN);
			Date date = sdf.parse(dateStr);
			return date;
		} catch (Exception e) {
//			if(BuildConfig.DEBUG) {
//				e.printStackTrace();
//			}
			return null;
		}
	}

	public static Date getDateFromDateStringWithNoSeparator(String dateStr){
		if(CommonMethods.checkStringIsBlank(dateStr)) return null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.JAPAN);
			Date date = sdf.parse(dateStr);
			return date;
		} catch (Exception e) {
//			if(BuildConfig.DEBUG) {
//				e.printStackTrace();
//			}
			return null;
		}
	}

	public static String getNoSeparatorDateStringFromRowDateString(String dateStr){
		if(CommonMethods.checkStringIsBlank(dateStr)) return null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmssZZZ", Locale.JAPAN);
			Date date = sdf.parse(dateStr);
			sdf.applyPattern("yyyyMMddHHmmss");
			return sdf.format(date);
		} catch (Exception e) {
//			if(BuildConfig.DEBUG) {
//				e.printStackTrace();
//			}
			return null;
		}
	}

	//Stringのブランクチェック
	public static boolean checkStringIsBlank(String value){
		if(value == null || value.equals("")) return true;
		return false;
	}

	/**
	 * オンラインチェッkう
	 * @return
	 */
	public static boolean isOnline() {
		return isOnline(false);
//		return isOnline(true); //
	}
    public static boolean isOnline(boolean deep) {
		boolean airplanemode = Settings.System.getInt(ctx.getContentResolver(),
								Settings.System.AIRPLANE_MODE_ON,0) != 0;
		if(airplanemode) {
			return false;
		}
        if(deep) {
            accessNetwork = false;
            networkChecking = true;
            try {
                URL packageUrl = new URL("http://google.com");
                HttpAccesser http = new HttpAccesser(ctx, packageUrl, HttpAccesser.GET, false, new HttpAccesser.HttpAccessResponse() {
                    @Override
                    public boolean needHeader() {
                        return false;
                    }

                    @Override
                    public boolean header(URL url, Map<String, List<String>> headers, Exception e) {
                        return false;
                    }

                    @Override
                    public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser) {
                        boolean result = false;

                        if(accesser == null)  {
                            result = false;
                        }else{
                            if (response.getStatusLine().getStatusCode() == 200) {
                                accessNetwork = true;
                                result = true;
                            }
                        }
                        networkChecking = false;
                        return result;
                    }

                    @Override
                    public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser) {

                    }
                }, 10);
                @SuppressWarnings("unchecked")
                HashMap<String, String>[] params = new HashMap[1];
                params[0] = new HashMap<String, String>();
                http.execute(params);
                while (networkChecking) {
                    Thread.sleep(250);
                }
            }
            catch (Exception e) {
                accessNetwork = true;
                Log.d("CommonMethods#isOnline" , e.getLocalizedMessage());
            }
        }else{
            accessNetwork = true;
        }
        if(!accessNetwork) {
            return false;
        }
        ConnectivityManager cm = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if( ni != null ){
            return cm.getActiveNetworkInfo().isConnected();
        }
        return false;
    }

	public static boolean isForegrand(Context context) {
		ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
		if(Build.VERSION.SDK_INT >= 19) { //TODO: Kitkat以降
			List<RunningAppProcessInfo> processInfoList = am.getRunningAppProcesses();
			for(RunningAppProcessInfo info : processInfoList){
				if(info.processName.equals(context.getPackageName())){
					if( info.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND){
						return true;
					}
				}
			}
		}else{
			if (am.getRunningTasks(1).get(0).topActivity.getPackageName().equals(context.getPackageName())) {
				return true;
			}
		}
		return false;
	}

	public static final void commonAlert(Context context, String title, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setMessage(message);
		alertDialogBuilder.setPositiveButton("閉じる", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();;
			}
		});
		alertDialogBuilder.setCancelable(false);
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public static final void commonAlert(Context context, String jsonStr) {
		try {
			JSONObject json = new JSONObject(jsonStr);
			JSONObject resultJson = json.getJSONObject("result");
			StringBuffer message = new StringBuffer(resultJson.getString("message"));
			message.append("("+resultJson.getInt("code")+")");
			JSONObject detailObject = resultJson.getJSONObject("detail");
			if(detailObject != null) {
				Iterator<String> keys = detailObject.keys();
				while(keys.hasNext()) {
					String key = keys.next();
					message.append("\n");
					message.append(detailObject.getString(key));
				}
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			alertDialogBuilder.setTitle("エラー");
			alertDialogBuilder.setMessage(message.toString());
			alertDialogBuilder.setPositiveButton("閉じる", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();;
				}
			});
			alertDialogBuilder.setCancelable(false);
			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
		}
		catch (Exception e) {
			//TODO エラー表示
			CommonMethods.commonAlert(context, context.getString(R.string.unknown_error_title), context.getString(R.string.unknown_error_message)+"\n"+e.getClass().getName());
		}
	}

	public static int getVersionCode(Context context){
		PackageManager pm = context.getPackageManager();
		int versionCode = 0;
		try{
			PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), 0);
			versionCode = packageInfo.versionCode;
		}catch(Exception e){
			e.printStackTrace();
		}
		return versionCode;
	}

	public static String getVersionName(Context context) {
		PackageManager pm = context.getPackageManager();
		String versionName = "";
		try {
			PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), 0);
			versionName = packageInfo.versionName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return versionName;
	}

	public static void deleteAll(File f) {
		if(f.exists() == false) {
			return;
		}

		if(f.isFile()) {
			f.delete();
		} else if(f.isDirectory()){
			File[] files = f.listFiles();
			for(int i=0; i<files.length; i++) {
				deleteAll(files[i]);
			}
			f.delete();
		}
	}
}
