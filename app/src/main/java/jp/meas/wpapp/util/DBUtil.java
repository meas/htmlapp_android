package jp.meas.wpapp.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

import jp.meas.wpapp.R;

/**
 * Created by nobu on 2016/11/21.
 */

public class DBUtil extends SQLiteOpenHelper {

    static final String dbfile  = "common_data.db";
    static final int    version = 1;

    static abstract public class Table {
        static public String dateToString(java.util.Date date) {
            String result = null;
            if(date != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmssZ");
                result = sdf.format(date);
            }
            return result;
        }
        static public java.util.Date stringToDate(String dateStr) {
            java.util.Date result = null;
            if(dateStr != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
                try {
                    result = sdf.parse(dateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }
        abstract String toJson();
    }

    static public class DataTable extends Table{
        public int id=-1;
        public String king="";
        public String json="";
        public Timestamp sort=null;
        public Timestamp create=null;
        public Timestamp update=null;

        public String toJson() {
            StringBuffer result = new StringBuffer();
            result.append("{");
            result.append(String.format("\"id\":%d,", id));
            result.append(String.format("\"king\":\"%s\",", king));
            result.append(String.format("\"json\":%s,", json));
            result.append(String.format("\"sort\":\"%s\",", dateToString(sort)));
            result.append(String.format("\"create\":\"%s\",", dateToString(create)));
            result.append(String.format("\"update\":\"%s\"", dateToString(update)));
            result.append("}");
            return result.toString();
        }
    }

    static public class NotificationTable extends Table {
        public int id;
        public int localStrageId;
        public String title;
        public String text;
        public String subText;
        public Timestamp alertTime;
        public int deleteFlg;
        public int repeatFlg;
        public Timestamp create;
        public Timestamp update;

        public String toJson() {
            StringBuffer result = new StringBuffer();
            result.append("{");
            result.append(String.format("\"id\":%d,", id));
            result.append(String.format("\"localStrageId\":%d,", localStrageId));
            result.append(String.format("\"title\":\"%s\",", title));
            result.append(String.format("\"text\":\"%s\",", text));
            result.append(String.format("\"subText\":\"%s\",", subText));
            result.append(String.format("\"date\":\"%s\",", dateToString(alertTime)));
            result.append(String.format("\"deleteFlg\":%d,", deleteFlg));
            result.append(String.format("\"create\":\"%s\",", dateToString(create)));
            result.append(String.format("\"update\":\"%s\"", dateToString(update)));
            result.append("}");
            return result.toString();
        }
    }

    protected Context context_;

    public DBUtil(Context c) {
        super(c, dbfile, null, version);
        context_ = c;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(context_.getResources().getString(R.string.create_data_table));
            db.execSQL(context_.getResources().getString(R.string.create_notification_table));
        }
        catch (Exception e) {
            e.printStackTrace();
            //TODO エラー表示
            CommonMethods.commonAlert(context_, context_.getString(R.string.dberror_title), context_.getString(R.string.dberror_message));
        }
        finally {

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //テーブル設計が同じ場合は不要
    }

    @Override
    public void onDowngrade (SQLiteDatabase db, int oldVersion, int newVersion) {
        //不要
    }

    protected void dump() {
        //未実装
    }

    protected void restore() {
        //未実装
    }

    public DataTableAccesser getDataTableWritable() {
        DataTableAccesser dta = new DataTableAccesser(getWritableDatabase());
        return dta;
    }

    public DataTableAccesser getDataTableReadable() {
        DataTableAccesser dta = new DataTableAccesser(getReadableDatabase());
        return dta;
    }

    public NotificationTableAccesser getNotificationTableWritable() {
        NotificationTableAccesser nta = new NotificationTableAccesser(getWritableDatabase());
        return nta;
    }

    public NotificationTableAccesser getNotificationTableReadable() {
        NotificationTableAccesser nta = new NotificationTableAccesser(getReadableDatabase());
        return nta;
    }

    static public class DataTableAccesser {
        SQLiteDatabase db_ = null;
        public DataTableAccesser(SQLiteDatabase db) {
            db_ = db;
        }

        @Override
        protected void finalize() throws Throwable {
            try {
                super.finalize();
            }
            finally {
                if(db_ != null) {
                    db_.close();
                }
            }
        }

        public DataTable insert(String sql) {
            DataTable result = new DataTable();
            result.id = -1;
            Cursor cursor = null;
            try {
                db_.execSQL(sql);
                cursor = db_.rawQuery("select * from local_storage where ROWID = last_insert_rowid();", null);
                if(cursor.moveToFirst()) {
                    result.id = cursor.getInt(cursor.getColumnIndex("id"));
                    result.king = cursor.getString(cursor.getColumnIndex("data_kind"));
                    result.json = cursor.getString(cursor.getColumnIndex("data"));
                    result.sort = new Timestamp(cursor.getLong(cursor.getColumnIndex("sort_date")));
                    result.create = new Timestamp(cursor.getLong(cursor.getColumnIndex("created")));
                    result.update = new Timestamp(cursor.getLong(cursor.getColumnIndex("modified")));
                }
            }
            catch (Exception e) {
                result.id = -99;
                result.king = "";
                result.json = "{\"message\":\""+e.getMessage()+"\"}";
                result.sort = new Timestamp(0);
                result.create = new Timestamp(0);
                result.update = new Timestamp(0);
            }
            finally {
                if(cursor != null) {
                    cursor.close();
                }
            }
            return result;
        }

        public boolean update(String sql) {
            boolean result = false;
            try {
                db_.execSQL(sql);
                result = true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        public List<DataTable> select(String sql) {
            Vector<DataTable> result = new Vector<DataTable>();
            Cursor cursor = null;
            try {
                cursor = db_.rawQuery(sql, null);
                if(cursor.moveToFirst()) {
                    for(int i=0;i<cursor.getColumnCount();i++) {
                        DataTable data = new DataTable();
                        data.id = cursor.getInt(cursor.getColumnIndex("id"));
                        data.king = cursor.getString(cursor.getColumnIndex("data_kind"));
                        data.json = cursor.getString(cursor.getColumnIndex("data"));
                        data.sort = new Timestamp(cursor.getLong(cursor.getColumnIndex("sort_date")));
                        data.create = new Timestamp(cursor.getLong(cursor.getColumnIndex("created")));
                        data.update = new Timestamp(cursor.getLong(cursor.getColumnIndex("modified")));
                        result.add(data);
                        cursor.moveToNext();
                    }
                }
            }
            catch (Exception e) {
                DataTable data = new DataTable();
                data.id = -99;
                data.king = "";
                data.json = "{\"message\":\""+e.getMessage()+"\"}";
                data.sort = new Timestamp(0);
                data.create = new Timestamp(0);
                data.update = new Timestamp(0);
                result.add(data);
            }
            finally {
                if(cursor != null) {
                    cursor.close();
                }
            }
            return result;
        }
    }

    static public class NotificationTableAccesser {
        SQLiteDatabase db_ = null;
        public NotificationTableAccesser(SQLiteDatabase db) {
            db_ = db;
        }

        @Override
        protected void finalize() throws Throwable {
            try {
                super.finalize();
            }
            finally {
                if(db_ != null) {
                    db_.close();
                }
            }
        }

        public NotificationTable insert(int localStrageId, String title, String text, String subText, String notificationDate, boolean repeat) {
            NotificationTable result = new NotificationTable();
            result.id = -1;
            Cursor cursor = null;
            long rowId = -1;
            try {
                SQLiteStatement st = null;
                try {
                    st = db_.compileStatement("insert into local_notification(local_storage_id, title, maintext, subtext, date, delete_flg, repeat_flg, created, modified) values(?, ?, ?, ? ,?, 0, ?, datetime('now'), datetime('now'))");
                    if(localStrageId >= 0) {
                        st.bindLong(1, localStrageId);
                    }else{
                        st.bindLong(1, -1);
                    }
                    st.bindString(2, title);
                    st.bindString(3, text);
                    st.bindString(4, subText);
                    st.bindLong(5, Table.stringToDate(notificationDate).getTime());
                    if(repeat) {
                        st.bindLong(6, 1);
                    }else{
                        st.bindLong(6, 0);
                    }
                    rowId =  st.executeInsert();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    result.id = -99;
                }
                finally {
                    if(st != null) {
                        st.close();
                    }
                }
                if(rowId >= 0) {
                    cursor = db_.rawQuery("select * from local_notification where ROWID = "+rowId+";", null);
                    if(cursor.moveToFirst()) {
                        result.id = cursor.getInt(cursor.getColumnIndex("id"));
                        result.localStrageId = cursor.getInt(cursor.getColumnIndex("local_storage_id"));
                        result.title = cursor.getString(cursor.getColumnIndex("title"));
                        result.text = cursor.getString(cursor.getColumnIndex("maintext"));
                        result.subText = cursor.getString(cursor.getColumnIndex("subtext"));
                        result.alertTime = new Timestamp(cursor.getLong(cursor.getColumnIndex("date")));
                        result.repeatFlg = cursor.getInt(cursor.getColumnIndex("repeat_flg"));
                        result.create = new Timestamp(cursor.getLong(cursor.getColumnIndex("created")));
                        result.update = new Timestamp(cursor.getLong(cursor.getColumnIndex("modified")));
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                result.id = -98;
            }
            finally {
                if(cursor != null) {
                    cursor.close();
                }
            }
            return result;
        }

        public int update(int id, int localStrageId, String title, String text, String subText, String notificationDate, boolean repeat) {
            int result = -1;
            SQLiteStatement st = null;
            try {
                st = db_.compileStatement("update local_notification set local_storage_id=?, title=?, maintext=?, subtext=?, date=?, repeat_flg=?, modified=datetime('now') where id = ?");
                st.bindLong(1, localStrageId);
                st.bindString(2, title);
                st.bindString(3, text);
                st.bindString(4, subText);
                st.bindLong(5, Table.stringToDate(notificationDate).getTime());
                if(repeat) {
                    st.bindLong(6, 1);
                }else{
                    st.bindLong(6, 0);
                }
                st.bindLong(7, id);
                result = st.executeUpdateDelete();
            }
            catch (Exception e) {
                result = -99;
            }
            finally {
                if(st != null) {
                    st.close();
                }
            }
            return result;
        }

        public int delete(int id) {
            SQLiteStatement st = null;
            int result = -1;
            try {
                st = db_.compileStatement("update local_notification set delete_flg=1, modified=datetime('now') where id = ?");
                st.bindLong(1, id);
                result = st.executeUpdateDelete();
            }
            catch (Exception e) {
                result = -1;
            }
            finally {
                if(st != null) {
                    st.close();
                }
            }
            return result;
        }

        public List<NotificationTable> select(String sql) {
            Vector<NotificationTable> result = new Vector<NotificationTable>();
            Cursor cursor = null;
            try {
                cursor = db_.rawQuery(sql, null);
                if(cursor.moveToFirst()) {
                    for(int i=0;i<cursor.getColumnCount();i++) {
                        NotificationTable data = new NotificationTable();
                        data.id = cursor.getInt(cursor.getColumnIndex("id"));
                        data.localStrageId = cursor.getInt(cursor.getColumnIndex("local_storage_id"));
                        data.title = cursor.getString(cursor.getColumnIndex("title"));
                        data.text = cursor.getString(cursor.getColumnIndex("maintext"));
                        data.subText = cursor.getString(cursor.getColumnIndex("subtext"));
                        data.alertTime = new Timestamp(cursor.getLong(cursor.getColumnIndex("date")));
                        data.repeatFlg = cursor.getInt(cursor.getColumnIndex("repeat_flg"));
                        data.create = new Timestamp(cursor.getLong(cursor.getColumnIndex("created")));
                        data.update = new Timestamp(cursor.getLong(cursor.getColumnIndex("modified")));
                        result.add(data);
                        cursor.moveToNext();
                    }
                }
            }
            catch (Exception e) {
            }
            finally {
                if(cursor != null) {
                    cursor.close();
                }
            }
            return result;
        }
    }
}
