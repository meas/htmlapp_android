package jp.meas.wpapp;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import org.apache.http.protocol.HttpService;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Timer;

import jp.meas.wpapp.container.UpdateEvent;
import jp.meas.wpapp.container.UpdateUIEvent;
import jp.meas.wpapp.server.DeviceRegisterTimerTask;
import jp.meas.wpapp.server.HttpServer;
import jp.meas.wpapp.server.HttpdService;
import jp.meas.wpapp.server.NotificationManageService;
import jp.meas.wpapp.util.CommonMethods;
import jp.meas.wpapp.util.StorageAccesser;
import jp.meas.wpapp.views.BaseWebAvtivity;
import jp.meas.wpapp.views.util.WebViewClientEx;

import static jp.meas.wpapp.views.util.WebViewClientEx.NOTIFICATION_EXTRA_KEY;

public class MainActivity extends BaseWebAvtivity implements ServiceConnection, UpdateUIEvent.UpdateUIEventLister {

    public static final String LATEST_LAOUNCH_VERSION = "latestLaunch";
	private Messenger messenger_ = null;
    static public Messenger messengerNotificaiton_ = null;
	private UpdateUIEvent updateUIEvent_ = null;
	private Timer registerTimer = null;
    private boolean callJsFunc = false;
    private boolean uiUpdating_ = false;
    private Uri schemeURI = null;
    private Intent serviceIntet = null;
    private Animation anim = null;
    private boolean nowLoadingHTML_ = false;
    private boolean noreload_ = false;
    private boolean uiUpdate = false;
    private boolean forceUpdate_=false;
    private ImageView splashView;

	static class ReplyHandler extends Handler {
		private Context context_=null;
		
		public ReplyHandler(Context context) {
			context_ = context;
		}
		
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			//TODO Message処理
		}
	}
	
	protected void launchService() {

		ActivityManager am = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
		List<RunningServiceInfo>services = am.getRunningServices(Integer.MAX_VALUE);
		boolean isActive = false;
		for(RunningServiceInfo service : services) {
			if(service.service.getClassName().equals(HttpService.class.getName())) {
				isActive = true;
				break;
			}
		}
		Log.v("DEBUG", "launchService " + Boolean.toString(isActive));
		if(!isActive) {
            serviceIntet = new Intent(this, HttpdService.class);
			startService(serviceIntet);
		}

        if(NotificationManageService.intent_ == null) {
            NotificationManageService.intent_ = new Intent(this, NotificationManageService.class);
            startService(NotificationManageService.intent_);
        }
	}

	@Override
    protected void onNewIntent(Intent intent) {
    	super.onNewIntent(intent);
    	setIntent(intent);
    }
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        hander_ = new Handler();
		StorageAccesser.initialize(this);
		CommonMethods.initialize(this);
		CommonMethods.createRootAndCacheDirectory();

        if(serviceIntet != null) {
            unbindService(this);
            stopService(serviceIntet);
        }
		//サービス起動
		launchService();
		bindService(new Intent(HttpdService.class.getName()), this, Context.BIND_AUTO_CREATE);
        bindService(new Intent(NotificationManageService.class.getName()), this, Context.BIND_AUTO_CREATE);

        //スキーム呼び出しの際のデータ取得
        schemeURI = getIntent().getData();
        //
		setContentView(R.layout.activity_main);
        initWebView();
        splashView = (ImageView)findViewById(R.id.splash);
        //TODO スプラッシュオン
        if(splashView.getVisibility() != View.VISIBLE) {
            splashView.setVisibility(View.VISIBLE);
        }
        forceUpdate_ = true;
		webView_.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                String scheme = uri.getScheme();
                String domain = uri.getHost();
                int port = Uri.parse(view.getUrl()).getPort();
                if (scheme.equals("updcon")) {
                    if (CommonMethods.isOnline()) {
                        if (!uiUpdating_) {
                            //TODO スプラッシュオン
                            if (splashView.getVisibility() != View.VISIBLE) {
                                splashView.setVisibility(View.VISIBLE);
                            }
                            contentGet(false);
                        }
                    }
                    return true;
                }else{
                    WebViewClientEx we = new WebViewClientEx(MainActivity.this);
                    if(we.shouldOverrideUrlLoading(view, url)) {
                        noreload_ = true;
                        return true;
                    }
                }
		        return super.shouldOverrideUrlLoading(view, url);
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.d("MainActivity", "onPageStarted");
                nowLoadingHTML_ = true;
            }
            @Override
            public void onPageFinished(WebView view , String url){
                Log.d("MainActivity", "onPageFinished");
                nowLoadingHTML_ = false;
                WebViewClientEx we = new WebViewClientEx(MainActivity.this);
                we.onPageFinished(view, url);
                if(callJsFunc) {
                    callJsFunc = false;
                    if(!nowContentsDownloading()) {
                        showRefreshDialog("webview done");
                    }
                }
                if(schemeURI != null) {
                    //スキーム呼び出し
                    if(Build.VERSION.SDK_INT >= 19) {
                        webView_.evaluateJavascript(String.format("javascript:launchOther('%s')", schemeURI.toString()), null);
                    }else{
                        webView_.loadUrl(String.format("javascript:launchOther('%s')", schemeURI.toString()));
                    }
                    schemeURI = null;
                }
            }
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.d("MainActivity", "onReceivedError");
            }
            @Override
            public void onLoadResource(WebView view, String url) {
                Log.d("MainActivity", "onLoadResource");
            }
		});
        anim = new ScaleAnimation(1f, 0.8f, 1f, 0.8f);
        anim.setDuration(200);
        anim.setFillAfter(false);
	}

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        webView_.saveState(outState);
    }
    @Override
    public void onRestoreInstanceState(Bundle outState){
        super.onRestoreInstanceState(outState);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

	@Override
	public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
        Log.v("DEBUG", "onServiceConnected");
        if(name.getClassName().equals("jp.meas.wpapp.server.HttpdService")) {
            onHttpServiceConntected(name, service);
        }else if(name.getClassName().equals("jp.meas.wpapp.server.NotificationManageService")) {
            onNotificatoinServiceConntected(name, service);
        }
	}

    protected void onHttpServiceConntected(android.content.ComponentName name, android.os.IBinder service) {
        messenger_ = new Messenger(service);
        updateUIEvent_ = new UpdateUIEvent(this, this);
        //文字列をリソースから持ってくるように変更
        String urlStr = getResources().getString(R.string.ui_package_url);
        String rootStr = getResources().getString(R.string.document_root);
        String cacheStr = getResources().getString(R.string.cache_path);
        String documentRoot = getFilesDir().getAbsolutePath() + rootStr + "/";
        String cachePath = getFilesDir().getAbsolutePath() + rootStr + "/" + cacheStr;

        try {
            updateUIEvent_.packageURL_ = new URL(urlStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        updateUIEvent_.documentRoot = documentRoot;
        updateUIEvent_.cachePath_ = cachePath;
        Bundle b = new Bundle();
        b.putSerializable("UpdateUIEvent", updateUIEvent_);
        Message msg = Message.obtain(null, UpdateEvent.EVENT_TYPE.UI_UPDATE.ordinal());
        msg.setData(b);
        try {
            messenger_.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
            //TODO エラー処理
            //TODO
            //TODO
            //TODO
            //TODO
            //TODO
        }
    }

    protected void onNotificatoinServiceConntected(android.content.ComponentName name, android.os.IBinder service) {
        messengerNotificaiton_ = new Messenger(service);
    }

	@Override
	public void onServiceDisconnected(android.content.ComponentName name) {
        Log.v("DEBUG", "onServiceDisConnected");
        if(name.getClassName().equals("jp.meas.wpapp.server.HttpdService")) {
            messenger_ = null;
        }else if(name.getClassName().equals("jp.meas.wpapp.server.NotificationManageService")) {
            messengerNotificaiton_ = null;
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
    @Override //毎起動時の処理
    protected void onResume() {
    	super.onResume();
        nowLoadingHTML_ = false;
        int notificationId = getIntent().getIntExtra(NOTIFICATION_EXTRA_KEY,-1);
        Log.d("DEBUG", String.format("通知からの起動>：%d", notificationId));
        if(notificationId > 0){
            getIntent().removeExtra(NOTIFICATION_EXTRA_KEY);
            //TODO
            //TODO
            //TODO
            //TODO ページ遷移するための呼び出し
            //TODO
            //TODO
            //TODO
            //TODO
            try {
                Message msgOff = Message.obtain(null, 0, "notification");
                MainActivity.messengerNotificaiton_.send(msgOff);
                Message msgOn = Message.obtain(null, 1, "notification");
                MainActivity.messengerNotificaiton_.send(msgOn);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        if(!noreload_) {
            String filePath = CommonMethods.getCashRootPath();
            File file = new File(filePath);
            boolean hasCache = file.exists() && file.listFiles().length > 0;
            if(!CommonMethods.isOnline()) {
                String dID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
                String uID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_UNIQUE_ID);
                if(hasCache && dID != null && uID != null) {
//                    webView_.clearCache(true);
//                    webView_.loadUrl(String.format("http://localhost:%d/index.html", HttpdService.serverPort));
                }else{
                    offlineDialog();
                }
                return;
            }

            if(StorageAccesser.isDeviceRegistered() == false){
                registDevice();
            } else {
                if(hasCache) {
                    if(CommonMethods.isOnline()) {
                        contentGet(false);
                        return;
                    }
                    //TODO スプラッシュオン
                    if(splashView.getVisibility() != View.VISIBLE) {
                        splashView.setVisibility(View.VISIBLE);
                    }
                    // 起動時にコンテンツ取得できない状況
                    callJsFunc = true;
                    webView_.clearCache(true);
                    webView_.loadUrl(String.format("http://localhost:%d/index.html", HttpdService.serverPort));
                }else{
                    contentGet(true);
                }
            }
        }
        noreload_ = false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }
    @Override
    public void onLongPress(MotionEvent e) {
    }
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }
    @Override
    public void onShowPress(MotionEvent e) {
    }
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);

        float absX = Math.abs(velocityX);
        float absY = Math.abs(velocityY);
        float distance = displayMetrics.widthPixels > 720 ? 3500f : 1800f;
        float pointY = displayMetrics.heightPixels - e1.getY();

        if(pointY > 230f) {
            if(velocityX > 0) { //swipeRight
                if(absX >= distance && absY < distance) {
                    if(Build.VERSION.SDK_INT >= 19) {
                        webView_.evaluateJavascript("javascript:swipeRight()", null);
                    }else{
                        webView_.loadUrl("javascript:swipeRight()");
                    }
                }
            }else{ //swipeLeft
                if(absX >= distance && absY < distance) {
                    if(Build.VERSION.SDK_INT >= 19) {
                        webView_.evaluateJavascript("javascript:swipeLeft()", null);
                    }else{
                        webView_.loadUrl("javascript:swipeLeft()");
                    }
                }
            }
        }
        return false;
    }

    private void offlineDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("通信状態を確認して下さい。");
        alertDialogBuilder.setPositiveButton("終了する", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //終了
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void registDevice() {
        DeviceRegisterTimerTask task = new DeviceRegisterTimerTask();
        task.setListeners(new DeviceRegisterTimerTask.DeviceRegisterTimerListener() {
            @Override
            public void deviceRegisterFinished(boolean success, String result) {
                //TODO push通知用のサーバ登録監視　未構築なのでエラーを一時的に無視
                Log.v("DEBUG", "resgister success");
                StorageAccesser.setDeviceRegistered(true);
                stopRegisterTimer();
                contentGet(true);
            }
        }, this);
        this.registerTimer = new Timer();
	    //FCMからレスポンスが戻ってくるまでは１秒毎に繰り返し。
        this.registerTimer.schedule(task, 200, 1000);

    }

    private boolean checkUpdate() {
        StorageAccesser sa = StorageAccesser.getInstance(this);
        String latestLaunchVer = sa.getSharedPreferences(LATEST_LAOUNCH_VERSION);
        String thisVersion = CommonMethods.getVersionName(this);
        if(latestLaunchVer == null || !thisVersion.equals(latestLaunchVer)) {
            return true;
        }
        return false;
    }

    private void contentGet(boolean execLocal) {
        if(!uiUpdating_) {
            uiUpdating_ = true;
            final UpdateUIEvent uiEvent = new UpdateUIEvent(this, new UpdateUIEvent.UpdateUIEventLister() {
                @Override
                public void updateUI(boolean success) {
                    //NOP
                }
            });
            String urlStr = getResources().getString(R.string.ui_package_url);
            String rootStr = getResources().getString(R.string.document_root);
            String cacheStr = getResources().getString(R.string.cache_path);
            String documentRoot = CommonMethods.getDocumentRootPath();
            File f = new File(documentRoot);
            String cachePath = CommonMethods.getCashRootPath();

            //ローカルのui.zipを反映
            if((execLocal && !f.exists()) || checkUpdate()){ //TODO 要確認 2017.01.29
                if(f.exists()) {
                    CommonMethods.deleteAll(f);
                }
                HttpServer.openLocalZip(this, true);
                StorageAccesser sa = StorageAccesser.getInstance(this);
                String thisVersion = CommonMethods.getVersionName(this);
                sa.setSharedPreferences(LATEST_LAOUNCH_VERSION, thisVersion);
            }

            try {
                uiEvent.packageURL_ = new URL(urlStr);
                uiEvent.documentRoot = documentRoot;
                uiEvent.cachePath_ = cachePath;
                uiEvent.execEvent(new UpdateEvent.EventCallbackListener() {
                    @Override
                    public void eventDone() {
                        webView_.clearCache(true);
                        // 読み込むタイミングで設定
                        nowLoadingHTML_ = true;
                        if(webView_.getUrl() == null) {
                            callJsFunc = true;
                            webView_.loadUrl(String.format("http://localhost:%d/index.html", HttpdService.serverPort));
                        }else{
                            //TODO ブラウザを更新しないので、ここでダイアログを消す
                            hander_.post(new Runnable() {
                                @Override
                                public void run() {
                                    //TODO スプラッシュオフ
                                    if(splashView.getVisibility() != View.GONE) {
                                        splashView.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                        uiUpdate = uiEvent.isUpdateUI;
                        showRefreshDialog("コンテンツ更新");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                webView_.clearCache(true);
                // 読み込むタイミングで設定
                callJsFunc = true;
                webView_.loadUrl(String.format("http://localhost:%d/index.html", HttpdService.serverPort));
                showRefreshDialog("コンテンツ更新");
            }
            uiUpdating_ = false;

            //設定取得
//            SettingActivity.getSettingInfo(getApplication(), new SaveSetting());
        }
    }

	@Override
	public void onStop(){
        webView_.loadUrl("javascript:terminateMovie()");
		//デバイス登録監視用タイマーの強制停止
		stopRegisterTimer();
		super.onStop();
	}


	@Override
	protected void onDestroy() {
        webView_.loadUrl("javascript:terminateMovie()");
		unbindService(this);
        if(serviceIntet != null) {
            stopService(serviceIntet);
            serviceIntet = null;
        }

        //通知サービスは止めない
//        if(NotificationManageService.intent_ != null) {
//            stopService(NotificationManageService.intent_);
//            NotificationManageService.intent_ = null;
//        }
		super.onDestroy();
	}

	//デバイス登録監視用タイマーの停止
	protected void stopRegisterTimer(){
		if(registerTimer != null) {
			registerTimer.cancel();
			registerTimer = null;
		}
	}

	//更新お知らせダイアログ　重ねがけ不許可
	protected void showRefreshDialog(final String alertTitle){
        Log.v("MainActivity", "showRefreshDialog: " + alertTitle);
        int delay = 600;

        if(callJsFunc) {
            //TODO コンテンツロード中
            hander_.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //TODO スプラッシュオフ
                    if(splashView.getVisibility() != View.GONE) {
                        splashView.setVisibility(View.GONE);
                    }
                }
            }, delay);
            return;
        }
        if(forceUpdate_ || uiUpdate) {
            forceUpdate_ = false;
            String dID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
            String uID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_UNIQUE_ID);
            String osVersion = String.format("%d", Build.VERSION.SDK_INT);
            String appVersion = "";
            PackageManager pm = getPackageManager();
            try {
                PackageInfo packageInfo = pm.getPackageInfo(getPackageName(), 0);
                appVersion = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                Log.d("DEBUG", "アプリバージョン名取得エラー");
            }
            webView_.clearCache(true);

            if(Build.VERSION.SDK_INT >= 19) {
                if(BuildConfig.DEBUG){
                    webView_.evaluateJavascript(String.format("javascript:setIds('%s', '%s', 'android', '%s', '%s', 'true')", dID, uID, osVersion, appVersion), null);
                }else{
                    webView_.evaluateJavascript(String.format("javascript:setIds('%s', '%s', 'android', '%s', '%s', 'false')", dID, uID, osVersion, appVersion), null);
                }
                delay = 600;
            }else{
                if(BuildConfig.DEBUG){
                    webView_.loadUrl(String.format("javascript:setIds('%s', '%s', 'android', '%s', '%s', 'true')", dID, uID, osVersion, appVersion));
                }else {
                    webView_.loadUrl(String.format("javascript:setIds('%s', '%s', 'android', '%s', '%s', 'false')", dID, uID, osVersion, appVersion));
                }
                delay = 1200;
            }
        }

        hander_.postDelayed(new Runnable() {
            @Override
            public void run() {
                //TODO スプラッシュオフ
                if(splashView.getVisibility() != View.GONE) {
                    splashView.setVisibility(View.GONE);
                }
            }
        }, delay);
	}


	public void updateUI(boolean success) {
		Log.v("DEBUG", "updateUI " + Boolean.toString(success));
        showRefreshDialog("コンテンツ更新");
	}

    protected boolean nowContentsDownloading() {
        if(uiUpdating_) {
            return true;
        }
        return false;
    }

	// WebViewインターフェース
	public void addBackgroundDownload() {
		//TODO
		/*
        Bundle b = new Bundle();
        b.putSerializable("UpdateUIEvent", updateUIEvent_);
        Message msg = Message.obtain(null, UpdateEvent.EVENT_TYPE.UI_UPDATE.ordinal());
        msg.setData(b);
        try {
			messenger_.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		 */
	}
}
