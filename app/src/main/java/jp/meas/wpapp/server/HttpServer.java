package jp.meas.wpapp.server;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import fi.iki.elonen.NanoHTTPD;
import jp.meas.wpapp.R;

public class HttpServer extends NanoHTTPD {

	protected Context context = null;

	public HttpServer(int port, Context context) {
		super(port);
		this.context = context;
	}

	@Override
	public Response serve(IHTTPSession session) {
		Map<String, String> header = session.getHeaders();
		Map<String, String> parms = session.getParms();
		String uri = session.getUri();
		return createResponse(uri, session.getMethod(), header, parms);
	}

	public Response createResponse(String uri, Method method, Map<String, String> header, Map<String ,String> parameters) {
		String ext = uri.lastIndexOf(".") > 0 ? uri.substring(uri.lastIndexOf(".")+1).toLowerCase() : "";
		String mimeType = "text/html";
		if(ext.equals("html")) {
			mimeType = "text/html";
		}else if(ext.equals("htm")) {
			mimeType = "text/html";
		}else if(ext.equals("xhtml")) {
			mimeType = "text/html";
		}else if(ext.equals("js")) {
			mimeType = "text/javascript";
		}else if(ext.equals("css")) {
			mimeType = "text/css";
		}else if(ext.equals("txt")) {
			mimeType = "text/plain";
		}else if(ext.equals("xml")) {
			mimeType = "text/xml";
		}else if(ext.equals("php")) {
			mimeType = "text/html";
		}else if(ext.equals("cgi")) {
			mimeType = "text/html";
		}else if(ext.equals("vbs")) {
			mimeType = "text/html";
		}else if(ext.equals("gif")) {
			mimeType = "image/gif";
		}else if(ext.equals("jpg")) {
			mimeType = "image/jpeg";
		}else if(ext.equals("jpeg")) {
			mimeType = "image/jpeg";
		}else if(ext.equals("png")) {
			mimeType = "image/png";
		}else if(ext.equals("ico")) {
			mimeType = "image/vnd.microsoft.icon";
		}else if(ext.equals("swf")) {
			mimeType = "application/x-shockwave-flash";
		}else if(ext.equals("mpg")) {
			mimeType = "video/mpeg";
		}else if(ext.equals("mpeg")) {
			mimeType = "video/mpeg";
		}else if(ext.equals("mp4")) {
			mimeType = "video/mp4";
		}else if(ext.equals("webm")) {
			mimeType = "video/webm";
		}else if(ext.equals("ogv")) {
			mimeType = "video/ogg";
		}else if(ext.equals("mov")) {
			mimeType = "video/quicktime";
		}else if(ext.equals("mp3")) {
			mimeType = "audio/mpeg";
		}else if(ext.equals("m4a")) {
			mimeType = "audio/aac";
		}else if(ext.equals("ogg")) {
			mimeType = "audio/ogg";
		}else if(ext.equals("mid")) {
			mimeType = "audio/midi";
		}else if(ext.equals("wav")) {
			mimeType = "audio/wav";
		}else if(ext.equals("zip")) {
			mimeType = "application/zip";
		}else if(ext.equals("pdf")) {
			mimeType = "application/pdf";
		}else if(ext.equals("doc")) {
			mimeType = "application/msword";
		}else if(ext.equals("docx")) {
			mimeType = "application/msword";
		}else if(ext.equals("xls")) {
			mimeType = "application/msexcel";
		}else if(ext.equals("xlsx")) {
			mimeType = "application/msexcel";
		}

		HttpServer.openLocalZip(this.context, false);
		String documentRootName = context.getString(R.string.document_root);
		String documentRootPath = context.getFilesDir().getAbsolutePath() + "/"+documentRootName+"/";
		String path = uri.startsWith("http:") ? uri.substring(uri.indexOf("/", "http://".length()+1)) : uri.substring(uri.indexOf("/")+1);
		if(path.trim().endsWith("/")) {
			path = path.trim()+"index.html";
		}
		File checkPath = new File(documentRootPath + path);
		if(checkPath.exists()) {
			try {
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(documentRootPath + path));
				return newChunkedResponse(NanoHTTPD.Response.Status.OK, mimeType, bis);
			}
			catch(Exception e) {
				return newFixedLengthResponse(NanoHTTPD.Response.Status.BAD_REQUEST, "text/html", "<html><body>"+e.getMessage()+"</body></html>");
			}
			
		}
		return newFixedLengthResponse(NanoHTTPD.Response.Status.NOT_FOUND, "text/html", "<html><body>content not found.</body></html>");
	}

	private static void removeDocumentRoot(File file) {
		if(file.isDirectory()) {
			File[] files = file.listFiles();
			for(File f : files) {
				removeDocumentRoot(f);
			}
		}else{
			file.delete();
		}
	}

	public static void openLocalZip(Context context, boolean fource) {
		String documentRootName = context.getString(R.string.document_root);
		AssetManager am = context.getResources().getAssets();
		String documentRootPath = context.getFilesDir().getAbsolutePath() + "/"+documentRootName+"/";
		File documentRoot = new File(documentRootPath);
		//rootを何処かで作ると見なくなるのでindex.htmlの有無でチェック
		//ここでrootを作るのを最初にすると作成イベントを取りづらい
		//_cacheが先に作られる可能性もある。
		if(fource && documentRoot.exists()) {
			removeDocumentRoot(documentRoot);
		}
		if(!documentRoot.exists()) {
			documentRoot.mkdirs();
		}
		String indexPath = documentRootPath + "index.html";
		File mainFile = new File(indexPath);
		if(!mainFile.exists() || fource){
			try {
				ZipEntry zipEntry = null;
				int len = 0;
				BufferedOutputStream bos = null;
				ZipInputStream zis = new ZipInputStream(am.open(context.getString(R.string.ui_package_file)));
				while ((zipEntry = zis.getNextEntry()) != null) {
					try {
						File infile = new File(zipEntry.getName());
						if(zipEntry.isDirectory()) {
							File newDir = new File(documentRootPath + infile.getAbsolutePath());
							newDir.mkdirs();
						}else{
							bos = new BufferedOutputStream(new FileOutputStream(documentRootPath + infile.getAbsolutePath()));
							byte[] buffer = new byte[1024];
							while ((len = zis.read(buffer)) != -1) {
								bos.write(buffer, 0, len);
							}
						}

						zis.closeEntry();
						if(bos != null) {
							bos.flush();
							bos.close();
						}
					}
					catch(Exception e) {
						e.printStackTrace(System.err);
					}
					bos = null;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
