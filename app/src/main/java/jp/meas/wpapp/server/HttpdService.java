package jp.meas.wpapp.server;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;

import java.util.Timer;

import jp.meas.wpapp.container.UpdateEvent;
import jp.meas.wpapp.container.UpdateUIEvent;

public class HttpdService extends Service {

	static private HttpServer server_ = null;
	static private Messenger messenger_ = null;
	static private UpdateUIEvent updateUIEvent_ = null;
	static private Timer timer_ = null;
	static private EventTimerTask eventTimerTask = null;
	static public int serverPort = 10080;


	static class ServiceHandler extends Handler {
		private Context context_;
		
		public ServiceHandler(Context context) {
			context_ = context;
		}
		
		@Override
		public void handleMessage(Message msg) {
			UpdateEvent.EVENT_TYPE event = UpdateEvent.fromOrdinal(UpdateEvent.EVENT_TYPE.class, msg.what);
			eventTimerTask.setContext(context_);
			switch(event) {
				case UI_UPDATE:
				{
					Bundle b = msg.getData();
					updateUIEvent_ = (UpdateUIEvent)b.getSerializable("UpdateUIEvent");
					eventTimerTask.addUpdateEvent(updateUIEvent_);
					break;
				}
				default:
					super.handleMessage(msg);
					break;
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		if(messenger_ != null) {
			return messenger_.getBinder();
		}
		return null;
	}
	
	@Override
	public void onCreate() {
		if(server_ == null) {
			for(int i=0;i<1000;i++) {
				try {
					server_ = new HttpServer(serverPort, this);
					server_.start();
					break;
				} catch (Exception e) {
					serverPort++;
					e.printStackTrace();
				}
			}
			messenger_ = new Messenger(new ServiceHandler(getApplicationContext()));
			//TODO:バックグラウンドダウンロードを一時的に廃止 2015.06.07
			eventTimerTask = new EventTimerTask();
//			timer_ = new Timer(true);
//			timer_.schedule(eventTimerTask, 1000*60*2, 1000*60*20);
			//TODO : DEBUG
//			timer_.schedule(eventTimerTask, 1000*60, 1000*60);
			//TODO : DEBUG
		}
	}
	
	@Override
	public void onDestroy() {
		if(timer_!=null) {
			timer_.cancel();
			timer_ = null;
		}
		if(server_ != null) {
			server_.stop();
			server_ = null;
		}
		super.onDestroy();
	}
}
