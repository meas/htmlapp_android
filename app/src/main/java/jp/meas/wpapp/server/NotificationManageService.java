package jp.meas.wpapp.server;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.util.Calendar;
import java.util.List;

import jp.meas.wpapp.MainActivity;
import jp.meas.wpapp.R;
import jp.meas.wpapp.util.DBUtil;
import jp.meas.wpapp.views.util.WebViewClientEx;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;
import static jp.meas.wpapp.views.util.WebViewClientEx.NOTIFICATION_EXTRA_KEY;

/**
 * Created by nobu on 2016/12/12.
 */

public class NotificationManageService extends Service {

    public static final int NOTIFICATION_MANAGER_SERVICE_ID = Integer.MIN_VALUE;
    public static Intent intent_ = null;
    private Messenger messenger_;

    public static NotificationManageService self_ = null;
    protected int startaId_ = -1;
    protected boolean isForgraound_ = true;

    static class NotificationHandler extends Handler {
        private Context context_;

        public NotificationHandler(Context context) {
            context_ = context;
        }

        @Override
        public void handleMessage(Message msg) {
            int id = msg.what;
            String control = msg.obj.toString();

            AlarmManager alarmManager = (AlarmManager)context_.getSystemService(Context.ALARM_SERVICE);
            if("add".equals(control)) {
                try {
                    String sql = WebViewClientEx.decodeURL(String.format("select * from local_notification where id = %d", id));
                    DBUtil db = new DBUtil(context_);
                    DBUtil.NotificationTableAccesser nta = db.getNotificationTableReadable();
                    List<DBUtil.NotificationTable> datas = nta.select(sql);
                    if(datas.size() > 0) {
                        DBUtil.NotificationTable data = datas.get(0);
                        Intent intent = new Intent(context_, AlarmReceiver.class);
                        intent.putExtra(NOTIFICATION_EXTRA_KEY, data.id);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context_, data.id, intent, FLAG_CANCEL_CURRENT);
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(data.alertTime.getTime());
                        if(data.repeatFlg == 1) {
                            //TODO 繰り返し
                            long interval = 1000 * 60 * 60 * 24 * 7; //1week
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), interval, pendingIntent);
                            }else{
                                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), interval, pendingIntent);
                            }
                        }else{
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                            }else{
                                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                            }
                        }
                        Messenger reply = msg.replyTo;
                        Message replyMsg = Message.obtain(null, 0);
                        try {
                            reply.send(replyMsg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch(Exception e) {
                    Messenger reply = msg.replyTo;
                    Message replyMsg = Message.obtain(null, 99);
                    try {
                        reply.send(replyMsg);
                    } catch (RemoteException ex) {
                        e.printStackTrace();
                    }
                }
            }else if("mod".equals(control)) {
                try {
                    String sql = WebViewClientEx.decodeURL(String.format("select * from local_notification where id = %d", id));
                    DBUtil db = new DBUtil(context_);
                    DBUtil.NotificationTableAccesser nta = db.getNotificationTableReadable();
                    List<DBUtil.NotificationTable> datas = nta.select(sql);
                    if(datas.size() > 0) {
                        DBUtil.NotificationTable data = datas.get(0);
                        Intent intent = new Intent(context_, AlarmReceiver.class);
                        intent.putExtra(NOTIFICATION_EXTRA_KEY, data.id);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context_, data.id, intent, FLAG_CANCEL_CURRENT);
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(data.alertTime.getTime());
                        if(data.repeatFlg == 1) {
                            //TODO 繰り返し
                            long interval = 1000 * 60 * 60 * 24 * 7; //1week
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), interval, pendingIntent);
                            }else{
                                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), interval, pendingIntent);
                            }
                        }else{
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                            }else{
                                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                            }
                        }
                        Messenger reply = msg.replyTo;
                        Message replyMsg = Message.obtain(null, 1);
                        try {
                            reply.send(replyMsg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch(Exception e) {
                    Messenger reply = msg.replyTo;
                    Message replyMsg = Message.obtain(null, 99);
                    try {
                        reply.send(replyMsg);
                    } catch (RemoteException ex) {
                        e.printStackTrace();
                    }
                }
            }else if("notification".equals(control)) {
                if(id == 1) {//通知設定オン
                    self_.setForground(false);
                }else{//通知設定オフ
                    self_.removeForground();
                }
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger_.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        messenger_ = new Messenger(new NotificationHandler((getApplicationContext())));
        setAlermOn(getApplicationContext());
    }

    public static void setAlermOn(Context context) {
        Calendar now = Calendar.getInstance();
        String sql = WebViewClientEx.decodeURL(String.format("select * from local_notification where date > %d", now.getTimeInMillis()));
        DBUtil db = new DBUtil(context);
        DBUtil.NotificationTableAccesser nta = db.getNotificationTableReadable();
        List<DBUtil.NotificationTable> datas = nta.select(sql);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        for(DBUtil.NotificationTable data : datas) {
            if(data.deleteFlg != 1) {
                //削除
                Intent intentRemove = new Intent(context, AlarmReceiver.class);
                PendingIntent pendingIntentRemove = PendingIntent.getBroadcast(context, data.id, intentRemove, FLAG_CANCEL_CURRENT);
                pendingIntentRemove.cancel();
                alarmManager.cancel(pendingIntentRemove);

                //追加
                Intent intent = new Intent(context, AlarmReceiver.class);
                intent.putExtra(NOTIFICATION_EXTRA_KEY, data.id);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, data.id, intent, FLAG_CANCEL_CURRENT);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(data.alertTime.getTime());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                }else{
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        self_ = this;
        startaId_ = startId;
        isForgraound_ = AlarmReceiver.getNotificationStatus(this);
        setForground(true);
        return START_STICKY;
    }

    protected void removeForground() {
        if(startaId_ > 0 && isForgraound_) {
            stopForeground(true);
            Intent intentRemove = new Intent(getApplicationContext(), AlarmReceiver.class);
            PendingIntent pendingIntentRemove = PendingIntent.getBroadcast(getApplicationContext(), NOTIFICATION_MANAGER_SERVICE_ID, intentRemove, FLAG_CANCEL_CURRENT);
            pendingIntentRemove.cancel();
            isForgraound_ = false;
        }
    }
    protected void setForground(boolean startUp) {
        if(AlarmReceiver.getNotificationStatus(this) && (startUp || !isForgraound_)) {
            isForgraound_ = true;
            Intent launchIntent = new Intent(getApplicationContext(), MainActivity.class);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), NOTIFICATION_MANAGER_SERVICE_ID, launchIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            builder.setContentIntent(pendingIntent);
            builder.setSmallIcon(R.drawable.ic_invisible);
            Drawable largeIconDrawable = getResources().getDrawable(R.drawable.ic_launcher);
            Bitmap largeIconBitmap = ((BitmapDrawable)largeIconDrawable).getBitmap();
            builder.setLargeIcon(largeIconBitmap);
            builder.setContentTitle(getString(R.string.service_notification_title));
            builder.setContentText("");
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                builder.setPriority(Notification.PRIORITY_MIN);
            }
            startForeground(startaId_, builder.build());
            if(!startUp) {
                setAlermOn(getApplicationContext());
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NotificationManageService.intent_ = new Intent(this, NotificationManageService.class);
        startService(NotificationManageService.intent_);
    }
}
