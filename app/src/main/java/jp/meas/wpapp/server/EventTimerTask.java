package jp.meas.wpapp.server;

import android.content.Context;
import android.util.Log;

import java.util.TimerTask;
import java.util.Vector;

import jp.meas.wpapp.container.UpdateEvent;
import jp.meas.wpapp.util.CommonMethods;

public class EventTimerTask extends TimerTask implements UpdateEvent.EventCallbackListener {

	private final Vector<UpdateEvent> events = new Vector<UpdateEvent>();
	private int index = 0;
	private Context context_ = null;

	@Override
	public void run() {
		if(context_ != null && CommonMethods.isForegrand(context_)) {
			Log.v("EventTimerTask", "Now foregrand. through download process.");
			eventDone();
		}else{
			if(events.size() > 0 && index < 1) {
				startExec(index);
			}
		}
	}

	public void setContext(Context context) {
		context_ = context;
	}

	public void startExec(int idx) {
		UpdateEvent event = events.get(idx);
		event.execEvent(this);
	}

	public void addUpdateEvent(UpdateEvent event) {
		if(events.contains(event)) {
			events.remove(event);
		}
		events.add(event);
	}
	
	public void removeUpdateEvent(UpdateEvent event) {
		if(events.contains(event)) {
			events.remove(event);
		}
	}
	
	@Override
	public void eventDone() {
		index++;
		if(events.size() > index) {
			startExec(index);
		}else{
			index = 0;
		}
	}
}
