package jp.meas.wpapp.server;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.util.Calendar;
import java.util.List;

import jp.meas.wpapp.MainActivity;
import jp.meas.wpapp.R;
import jp.meas.wpapp.util.DBUtil;

import static jp.meas.wpapp.views.util.WebViewClientEx.NOTIFICATION_EXTRA_KEY;

/**
 * Created by nobu on 2016/11/23.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationId = intent.getIntExtra(NOTIFICATION_EXTRA_KEY,-1);
        Log.d("DEBUG", "------- 通知起動 -------");
        if(getNotificationStatus(context)) {
            String sql = String.format("select * from local_notification where id = %d;",notificationId);
            DBUtil db = new DBUtil(context);
            DBUtil.NotificationTableAccesser nta = db.getNotificationTableReadable();
            List<DBUtil.NotificationTable> datas = nta.select(sql);
            if(datas.size() > 0) {
                DBUtil.NotificationTable data = datas.get(0);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(data.alertTime);

//            Intent launchIntent = new Intent(Intent.ACTION_MAIN);
//            launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//            launchIntent.setClassName(context.getApplicationContext().getPackageName(), MainActivity.class.getName());
//            launchIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_NEW_TASK);
                Intent launchIntent = new Intent(context.getApplicationContext(), MainActivity.class);
                launchIntent.putExtra(NOTIFICATION_EXTRA_KEY, data.id);
                Log.d("DEBUG", String.format("------- 通知へID渡し：%d -------", data.id));

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext());
                PendingIntent pendingIntent = PendingIntent.getActivity(context, data.id, launchIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                builder.setContentIntent(pendingIntent);
                builder.setAutoCancel(true);

                builder.setSmallIcon(R.drawable.ic_launcher);
                builder.setContentTitle(data.title);
                builder.setContentText(data.text);
                builder.setSubText(data.subText);
                builder.setWhen(calendar.getTimeInMillis());
                builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);

                NotificationManagerCompat manager = NotificationManagerCompat.from(context.getApplicationContext());
//            manager.cancelAll();
                manager.notify(data.id, builder.build());

                // スクリーンオン
                PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyTag");
                wl.acquire(2000);
            }
        }
    }

    /**
     * 通知設定保存
     * @param context
     * @param on
     */
    public static void setNotifictionStatus(Context context, boolean on) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("notification_mode", on);
        editor.commit();
        Message msg = Message.obtain(null, on ? 1 : 0, "notification");
        try {
            MainActivity.messengerNotificaiton_.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通知設定取得
     * @param context
     * @return
     */
    public static boolean getNotificationStatus(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sharedPreferences.getBoolean("notification_mode", true);
    }
}
