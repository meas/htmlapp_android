package jp.meas.wpapp.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by nobu on 2016/12/12.
 */

public class UpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //TODO AlarmReviverの設定
        if(NotificationManageService.intent_ == null) {
            NotificationManageService.intent_ = new Intent(context, NotificationManageService.class);
            context.startService(NotificationManageService.intent_);
        }
        NotificationManageService.setAlermOn(context);
    }
}
