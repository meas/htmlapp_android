package jp.meas.wpapp.server;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

//AsyncTasc の第３引数をHttpResponse -> Stringへ
public class HttpAccesser extends AsyncTask<Map<String, String>, Float, String> {
	static final public int GET    = 0;
	static final public int POST   = 1;
	static final public int PUT    = 2;
	static final public int DELETE = 3;

	protected ProgressDialog progressDialog_;
	protected Context context_ = null;
	protected URL url_ = null;
	protected int method_ = GET;
	protected boolean viewProgress_ = false;
	protected HttpAccessResponse callback_;
	protected HttpResponse response_ = null;
	protected int priority_ = -1;
	public int tag = 0;
	public Boolean deepCheck_ = true;

	public static final boolean isOnline(Context context) {
		ConnectivityManager cm =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if(ni != null) {
			return  ni.isConnected();
		}

		return false;
	}

	//resultの戻り値をboolean
	//finiehed追加
	public interface HttpAccessResponse {
        public boolean needHeader();
		public boolean header(URL url, Map<String, List<String>> headers, Exception e);
		public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser);
		public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser);
	}
	
	public HttpAccesser(Context context, URL url, int method, boolean viewProgress, HttpAccessResponse callback) {
		context_ = context;
		url_ = url;
		method_ = method;
		viewProgress_ = viewProgress;
		callback_ = callback;
		priority_ = -1;
	}
	public HttpAccesser(Context context, URL url, int method, boolean viewProgress, HttpAccessResponse callback, int priority) {
		context_ = context;
		url_ = url;
		method_ = method;
		viewProgress_ = viewProgress;
		callback_ = callback;
		priority_ = priority;
	}

	public String getURLString(){
		if(url_ != null){
			return url_.toString();
		} else {
			return  null;
		}
	}

	@Override
	protected void onPreExecute() {
		if(viewProgress_) {
			progressDialog_ = new ProgressDialog(context_);
			progressDialog_.setMessage("サーバー通信中...");
			progressDialog_.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog_.show();
			
		}
	}

	private boolean getHeader() {
        boolean result = false;
		try {
			Map<String, List<String>> headers = null;
			if(url_.getProtocol().equals("https")) {
				HttpsURLConnection con = (HttpsURLConnection)url_.openConnection();
				con.setRequestMethod("HEAD");
				con.setInstanceFollowRedirects(false);
				con.connect();
				headers = con.getHeaderFields();
			}else{
				HttpURLConnection con = (HttpURLConnection)url_.openConnection();
				con.setRequestMethod("HEAD");
				con.setInstanceFollowRedirects(false);
				con.connect();
				headers = con.getHeaderFields();
			}
            result = callback_.header(url_, headers, null);
		}
		catch(Exception e) {
			e.printStackTrace(System.err);
            result = callback_.header(url_, null, e);
		}
		return result;
	}
	
	@Override
	//protected HttpResponse doInBackground(Map<String, String>... params) {
	protected String doInBackground(Map<String, String>... params) {

        boolean continueProcess = true;
		if(priority_ > 0) {
			Thread.currentThread().setPriority(priority_);
		}
        if(callback_.needHeader()) {
            continueProcess = getHeader();
        }
		HttpClient client = null;

        if(!continueProcess) return "cancel";

		try {
			client = new DefaultHttpClient();
			HttpUriRequest request= null;
			Map<String, String>param = params[0];
			if(method_ == POST) {
				ArrayList<NameValuePair> reqParam = new ArrayList<NameValuePair>();
				Set<String> keys = param.keySet();
				if (keys.size() > 1) {
					for (String key : keys) {
						String value = param.get(key);
						reqParam.add(new BasicNameValuePair(key, value));
					}
					request = new HttpPost(url_.toURI());
					request.addHeader("Content-Type", "application/x-www-form-urlencoded");
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(reqParam, "UTF-8"));
				} else if (keys.size() > 0) {
					String value = param.values().iterator().next();
					request = new HttpPost(url_.toURI());
					//jsonのみ
					request.addHeader("Accept", "application/json");
					request.addHeader("Content-Type", "application/json");
					((HttpPost) request).setEntity(new StringEntity(value,"UTF-8"));
				}
			}else if(method_ == PUT) {
				ArrayList<NameValuePair> reqParam = new ArrayList<NameValuePair>();
				Set<String> keys = param.keySet();
				if (keys.size() > 1) {
					for (String key : keys) {
						String value = param.get(key);
						reqParam.add(new BasicNameValuePair(key, value));
					}
					request = new HttpPut(url_.toURI());
					request.addHeader("Content-Type", "application/x-www-form-urlencoded");
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(reqParam, "UTF-8"));
				} else if (keys.size() > 0) {
					String value = param.values().iterator().next();
					request = new HttpPut(url_.toURI());
					//jsonのみ
					request.addHeader("Accept", "application/json");
					request.addHeader("Content-Type", "application/json");
					((HttpPost) request).setEntity(new StringEntity(value,"UTF-8"));
				}
			}else if(method_ == DELETE) {
				ArrayList<NameValuePair> reqParam = new ArrayList<NameValuePair>();
				Set<String> keys = param.keySet();
				if (keys.size() > 1) {
					for (String key : keys) {
						String value = param.get(key);
						reqParam.add(new BasicNameValuePair(key, value));
					}
					request = new HttpDelete(url_.toURI());
					request.addHeader("Content-Type", "application/x-www-form-urlencoded");
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(reqParam, "UTF-8"));
				} else if (keys.size() > 0) {
					String value = param.values().iterator().next();
					request = new HttpDelete(url_.toURI());
					//jsonのみ
					request.addHeader("Accept", "application/json");
					request.addHeader("Content-Type", "application/json");
					((HttpPost) request).setEntity(new StringEntity(value,"UTF-8"));
				}
			}else{
				StringBuffer query = new StringBuffer();
				query.append(url_);
				query.append("?");
				boolean first = true;
				Set<String> keys = param.keySet(); 
				for(String key : keys) {
					String value = param.get(key);
					if(!first) {
						query.append("&");
					}else{
						first = false;
					}
					query.append(URLEncoder.encode(key, "UTF-8"));
					query.append("=");
					query.append(URLEncoder.encode(value, "UTF-8"));
				}
				request = new HttpGet(query.toString());
			}
			HttpParams p = client.getParams();
			HttpConnectionParams.setConnectionTimeout(p, 10000);
			HttpConnectionParams.setSoTimeout(p, 10000);
			response_ = client.execute(request);
		}
		catch(Exception e) {
			if(viewProgress_) {
				progressDialog_.dismiss();
				progressDialog_ = null;
			}
			e.printStackTrace(System.err);
		}
		finally {
			//ここでクライアントを閉じると、InputStreamを読む時
			//Socketexception が発生する
			//if(client != null) {
			//	client.getConnectionManager().shutdown();
			//}
		}

		//callbackでストリーム操作をするとNetworkOnMainThreadExceptionが出る
		Boolean callBackSuccess = false;

		try {
			InputStream is = response_.getEntity().getContent();
			callBackSuccess = callback_.result(response_, is, context_, this);
		} catch (Exception e) {
			e.printStackTrace();
			callBackSuccess = callback_.result(response_, null, context_, this);
		}

		if(client != null) {
			client.getConnectionManager().shutdown();
		}

		if(callBackSuccess)
			return "success";
		else
			return "error";

		//return null;
		//Add
	}
	
	@Override
    protected void onProgressUpdate(Float... progress) {
		//
	}

	@Override
    protected void onPostExecute(String result) {
		if(viewProgress_) {
			progressDialog_.dismiss();
			progressDialog_ = null;
		}

		//Comment out
		callback_.finished(response_, result, context_, this);
		/*try {
			callback_.result(response_, response_.getEntity().getContent(), context_, this);
		} catch (IOException e) {

			callback_.result(response_, null, context_, this);
			e.printStackTrace();
		}*/
	}
	
	public void terminate() {
	}
}
