/**
 * Created by  on 3/23/15.
 * デバイス登録監視用タイマーオブジェクト
 */


package jp.meas.wpapp.server;

import android.content.Context;

import java.util.TimerTask;

import jp.meas.wpapp.container.DeviceRegisterEvent;
import jp.meas.wpapp.fcm.NotificationIdService;
import jp.meas.wpapp.util.StorageAccesser;

public class DeviceRegisterTimerTask extends TimerTask implements DeviceRegisterEvent.DeviceRegisterCallback{

	private Context context;
	private DeviceRegisterTimerListener listener;
	private boolean isRun = false;


	public interface DeviceRegisterTimerListener{
		void deviceRegisterFinished(boolean success, String result);
	}


	public void setListeners(DeviceRegisterTimerListener callback, Context ctx){
		this.listener = callback;
		this.context = ctx;
	}

	@Override
	public void run() {
		//デバイス登録がされているとgetRegistrationIdでトークンが取得できる
		String genID = NotificationIdService.deviceId;
		if(genID == null || genID.equals("")) return;
		if(isRun == true) return;
		isRun = true;
		StorageAccesser.setPreferenceString(StorageAccesser.KEY_DEVICE_ID, genID);
		//トークンをサーバーにPOST
		DeviceRegisterEvent registerEvent = new DeviceRegisterEvent(this.context);
		registerEvent.setRegisterCallback(this);
		registerEvent.execEvent(null);
	}

	@Override
	public void registerFinished(boolean success, String result) {
		if(this.listener != null){
			this.listener.deviceRegisterFinished(success, result);
            isRun = false;
		}
	}
}
