package jp.meas.wpapp.container;

import java.io.Serializable;

public abstract class UpdateEvent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4558529936856724877L;
	
	protected EventCallbackListener callback_ = null;
	
	public interface EventCallbackListener {
		public void eventDone();
	}

	public enum EVENT_TYPE {
		UI_UPDATE,
	}

	public static <E extends Enum<E>> E fromOrdinal(Class<E> enumClass, int ordinal) {
	    E[] enumArray = enumClass.getEnumConstants();
	    return enumArray[ordinal];
	}
	
	public abstract void execEvent(EventCallbackListener listener);
}
