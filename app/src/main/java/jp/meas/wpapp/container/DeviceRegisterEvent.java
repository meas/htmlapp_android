package jp.meas.wpapp.container;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.meas.wpapp.R;
import jp.meas.wpapp.server.HttpAccesser;
import jp.meas.wpapp.util.StorageAccesser;

/**
 * Created by nobu
 */
public class DeviceRegisterEvent extends UpdateEvent implements HttpAccesser.HttpAccessResponse{

	private Context context = null;
	private DeviceRegisterCallback rCallback = null;
	private String callbackMessage = "";

	public DeviceRegisterEvent(Context ctx){
		this.context = ctx;
	}

	public interface DeviceRegisterCallback{
		void registerFinished(boolean success, String result);
	}

	public void setRegisterCallback(DeviceRegisterCallback cb){
		this.rCallback = cb;
	}

	@Override
	public void execEvent(EventCallbackListener listener){
		callback_ = listener;
		String postUrl = context.getResources().getString(R.string.register_url);
		URL url = null;
		try {
			url = new URL(postUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			if(this.rCallback != null){
				this.rCallback.registerFinished(false, "register url is missing.");
			}
			return;
		}
		HttpAccesser http = new HttpAccesser(context, url, HttpAccesser.POST, false, this);
		HashMap<String, String>[] params = null;

		String dID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_DEVICE_ID);
		String uID = StorageAccesser.getPreferenceString(StorageAccesser.KEY_UNIQUE_ID);
		try {
			JSONObject regObj = new JSONObject();
			regObj.put("device_id", dID);
			regObj.put("unique_id", uID);
			regObj.put("device", "Android");
			regObj.put("ver", context.getString(R.string.api_version));
			JSONObject rootObj = new JSONObject();
			rootObj.put("regist", regObj);
			params = new HashMap[1];
			params[0] = new HashMap<String, String>();
			params[0].put("", rootObj.toString());
			http.execute(params);
		} catch (JSONException e) {
			e.printStackTrace();
			if(this.rCallback != null){
				this.rCallback.registerFinished(false, "couldn't make post params.");
			}
		}
	}

	@Override
	public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser) {
		if(response != null && response.getStatusLine().getStatusCode() == 200) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				StringBuffer buf = new StringBuffer();
				String str = null;
				while ((str = reader.readLine()) != null) {
					buf.append(str);
					buf.append("\n");
				}
				JSONObject resObj = new JSONObject(buf.toString());
				JSONObject resObj2 = resObj.getJSONObject("result");
				int resCode = resObj2.getInt("code");
				String resMessage = resObj2.getString("reason");

				is.close();

				Log.d("DEBUG", resObj.toString());
				callbackMessage = resMessage;
				if(resCode == 0) return true;
				else return false;
			} catch (IOException e) {
				e.printStackTrace();
				callbackMessage = e.getMessage();
				return false;
			} catch (JSONException e) {
				e.printStackTrace();
				callbackMessage = e.getMessage();
				return false;
			}
		} else {
			callbackMessage = "response is not 200";
			return false;
		}
	}

	@Override
	public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser) {
		if(this.rCallback != null) {
			if (result.equals("success")) {
				this.rCallback.registerFinished(true, callbackMessage);
			} else {
				this.rCallback.registerFinished(false, callbackMessage);
			}
		}
	}

	@Override
	public boolean needHeader() {
		return false;
	}

	@Override
	public boolean header(URL url, Map<String, List<String>> headers, Exception e) {
		return true;
	}
}
