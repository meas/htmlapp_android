package jp.meas.wpapp.container;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import jp.meas.wpapp.R;
import jp.meas.wpapp.server.HttpAccesser;
import jp.meas.wpapp.server.HttpAccesser.HttpAccessResponse;
import jp.meas.wpapp.util.StorageAccesser;

public class UpdateUIEvent extends UpdateEvent implements HttpAccessResponse {
	/**
	 * 
	 */
    public static boolean nowExecute = false;
	private static final long serialVersionUID = 2597179775019924556L;
	private String DATE_KEY = "ETag"; //"Last-Modified"より"ETag"に変更 2015.04.15;

	public interface UpdateUIEventLister {
		//success flag追加
		public void updateUI(boolean success);
	}
	
	private UpdateUIEventLister listener_ = null;
	private Context context_ = null;
	
	public URL packageURL_ = null;
	public String documentRoot = null;
	public String cachePath_ = null;
	public boolean isUpdateUI = false;
	
	public UpdateUIEvent(Context context, UpdateUIEventLister listener) {
		context_ = context;
		listener_ = listener;
	}

	public void execEvent(EventCallbackListener callback) {
        callback_ = callback;
        if(!nowExecute) {
            nowExecute = true;
            HttpAccesser http = new HttpAccesser(context_, packageURL_, HttpAccesser.GET, false, this);
            @SuppressWarnings("unchecked")
            HashMap<String, String>[] params = new HashMap[1];
            params[0] = new HashMap<String, String>();
            http.execute(params);
        }else{
            callback_.eventDone();
        }
	}

	//resultはdoInBackGroundの中から呼び出すコールバックなので非同期
	@Override
	public boolean result(HttpResponse response, InputStream is, Context context, HttpAccesser accesser) {
		if(accesser == null || response == null) {
			return false;
		}
		if(response.getStatusLine().getStatusCode() == 200) {
			StorageAccesser sa = StorageAccesser.getInstance(context_);
			String upUpdateStr = sa.getSharedPreferences(DATE_KEY);
			Header[] headers = response.getAllHeaders();
			String lastModified = null;
			for (Header header : headers) {
				if(header.getName().equals(DATE_KEY)) {
					lastModified = header.getValue();
					break;
				}
			}
			if(upUpdateStr == null || !upUpdateStr.equals(lastModified)) {
				isUpdateUI = true;
				String tempUIPath = context_.getCacheDir().getAbsolutePath() + "/aui.zip";
				File tempUI = new File(tempUIPath);
				if(tempUI.exists()) {
					tempUI.delete();
				}
				try {
					int len = 0;
					BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(tempUI));
		            byte[] buffer = new byte[1024];
		            while ((len = is.read(buffer)) != -1) {
		            	bos.write(buffer, 0, len);
		            }
	            	bos.flush();
		            bos.close();
		            bos = null;
					
		    		String documentRootName = context_.getString(R.string.document_root);
		    		AssetManager am = context_.getResources().getAssets();
		    		String documentRootPath = context_.getFilesDir().getAbsolutePath() + "/"+documentRootName+"/";
		    		File documentRoot = new File(documentRootPath);
	    			String cacheTempPath = String.format("%s/%s/", context_.getCacheDir().getAbsolutePath(), context_.getString(R.string.cache_path));
	    			File cacheTemp = new File(cacheTempPath);
					if(cacheTemp.exists()) {
						cacheTemp.delete();
					}
	    			String cachePath = String.format("%s%s/", documentRootPath, context_.getString(R.string.cache_path));
	    			File cache = new File(cachePath);
		    		if(documentRoot.exists()) {
						if(cache.exists()) {
							cache.renameTo(cacheTemp);
						}
		    		}else{
		    			cacheTemp.delete();
		    		}
		    		documentRoot.delete();
					documentRoot.mkdirs();

					ZipEntry zipEntry = null;
					len = 0;
					ZipInputStream zis = new ZipInputStream(new FileInputStream(tempUI));
			        while ((zipEntry = zis.getNextEntry()) != null) {
			            File infile = new File(zipEntry.getName());
			            if(zipEntry.isDirectory()) {
			            	File newDir = new File(documentRootPath + infile.getAbsolutePath());
			            	newDir.mkdirs();
			            }else{
				            bos = new BufferedOutputStream(new FileOutputStream(documentRootPath + infile.getAbsolutePath()));
				            while ((len = zis.read(buffer)) != -1) {
				            	bos.write(buffer, 0, len);
				            }
			            }
			 
			            zis.closeEntry();
			            if(bos != null) {
			            	bos.flush();
				            bos.close();
			            }
			            bos = null;
			        }
			        tempUI.delete();
			        if(cacheTemp.exists()) {
			        	cacheTemp.renameTo(cache);
			        	cacheTemp.delete();
			        }else{
						cache.mkdirs();
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				sa.setSharedPreferences(DATE_KEY, lastModified);
				//success ?
				return true;

			} else {
				//Comment アップデートの必要なしの場合なのでどうするのか？
				return true;
			}
		}else{
			//failed
//            Log.v("http error : ",  accesser.getURLString() +"("+ String.valueOf(response.getStatusLine().getStatusCode())+")");
            String urlStr = accesser.getURLString();
            Log.v("http error : ",  String.valueOf(response.getStatusLine().getStatusCode()));
            Log.v("URL : ",  urlStr);
			return false;

			// 何もしない
        }
		//Comment out
		//callback_.eventDone();
	}

	//OnMainThread
	@Override
	public void finished(HttpResponse response, String result, Context context, HttpAccesser accesser) {
        nowExecute = false;
		if(result.equals("success")){
			listener_.updateUI(true);
		} else {
			listener_.updateUI(false);
		}
		callback_.eventDone();
	}
	//Added End

	@Override
	public boolean needHeader() {
		return true;
	}

	@Override
	public boolean header(URL url, Map<String, List<String>> headers, Exception e) {
		boolean result = true;
		if(headers != null) {
			StorageAccesser sa = StorageAccesser.getInstance(context_);
			String upUpdateStr = sa.getSharedPreferences(DATE_KEY);
			List<String> etags = headers.get(DATE_KEY);
			if(upUpdateStr != null && etags != null && etags.size() > 0) {
				String etag = etags.get(0);
				if(upUpdateStr.equals(etag)) {
					result = false;
				}
			}
		}
		return result;
	}
}
